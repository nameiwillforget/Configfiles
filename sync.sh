#!/bin/bash
sudo cp -r my-mappings.yaml /etc/interception/dual-function-keys/ 
cp -r emacsconfig ~/.emacs.d/
cp -r stumpwmconfig ~/.stumpwm.d/
sudo cp daselt /usr/share/X11/xkb/symbols/ 

[ ! -d ~/.config/tridactyl/ ] && mkdir ~/.config/tridactyl

cp tridactylrc ~/.config/tridactyl/

cp ~/.emacs.d/emacsconfig/init.el ~/.emacs.d/

sed -i "/acpi-backlight/,/amdgpu/"' s/^/;;/' "stumpwmconfig/modules.lisp"
cp ~/.stumpwm.d/stumpwmconfig/init.lisp ~/.stumpwm.d/

cp stumpwm.ros ~/
sudo cp stumpwm.desktop /usr/share/xsessions/

cp -r bashscripts ~/
sudo ln ~/bashscripts/redaselt /bin/redaselt
