(defcommand set-backlight (level) ((:number "Set brightness level:"))
  (with-open-file (sys-backlight-file "/sys/class/backlight/amdgpu_bl2/brightness"
                                      :direction :output :if-exists :overwrite)
    (format sys-backlight-file "~a~%" (* 26 level))))

;; C-- [1-9] changes level
(defvar *brightness-map* nil)
(setf *brightness-map*
      (let ((b (make-sparse-keymap)))
        (loop for level from 0 below 10 by 1 do
             (define-key b (kbd (write-to-string level))
               (format nil "set-backlight ~a" level)))
        b))

(define-key *root-map* (kbd "-") '*brightness-map*)

;; min and max level set with media keys 
(define-key *top-map* (kbd "XF86MonBrightnessDown") "set-backlight 0")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "set-backlight 9")
