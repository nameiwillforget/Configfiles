;; Load swank.
;; *prefix-key* ; swank will kick this off
(load "/home/alex/.roswell/lisp/slime/2023.06.18/swank-loader.lisp")
(swank-loader:init)
(defcommand swank () ()
    (swank:create-server :port 4004
                       :style swank:*communication-style*
                       :dont-close t)
  (echo-string (current-screen) 
               "Starting swank. M-x slime-connect RET RET, then (in-package stumpwm)."))
(swank)
