;;-*-lisp-*-
(in-package :stumpwm)



(defvar al/init-directory
  (directory-namestring
   (truename "/home/alex/.stumpwm.d/stumpwmconfig"))
  "A directory with initially loaded files.")


(defun al/load (filename)
  "Load a file FILENAME (without extension) from `al/init-directory'."
  (let ((file (merge-pathnames (concat filename ".lisp")
                               al/init-directory)))
    (if (probe-file file)
        (load file)
        (format *error-output* "File '~a' doesn't exist." file))))

(redirect-all-output (merge-pathnames "log" al/init-directory))

(al/load "modules")
(al/load "modeline")
(al/load "xkblayout")
(al/load "commands")
(al/load "keys") 
(al/load "swank")
(al/load "screenshot-maim")
(al/load "settings")
