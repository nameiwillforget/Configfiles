(load-module "stump-nm")
(load-module "battery-portable")
(load-module "wifi")
(load-module "binwarp")
(load-module "spatial-groups")
;;(load-module "cpu")
(load-module "swm-emacs")

(load-module "winner-mode")
(add-hook *post-command-hook* (lambda (command)
                                (when (member command winner-mode:*default-commands*)
                                  (winner-mode:dump-group-to-file))))

(load-module "clipboard-history")
;; start the polling timer process
(clipboard-history:start-clipboard-manager)

(stumpwm:add-to-load-path "~/.stumpwm.d/modules/pamixer")
(load-module "pamixer")

(stumpwm:add-to-load-path "~/.stumpwm.d/modules/screenshot-maim")
(load-module "screenshot-maim")

(stumpwm:add-to-load-path "~/.stumpwm.d/modules/acpi-backlight")
(load-module "acpi-backlight")
(acpi-backlight:init "amdgpu_bl2") ; use name of your ACPI backlight device
