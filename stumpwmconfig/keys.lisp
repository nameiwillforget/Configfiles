;; Initialize keymaps
(defparameter *emacs-map* (make-sparse-keymap))
(defparameter *run-app-map* (make-sparse-keymap))
(defparameter *quit-map* (make-sparse-keymap))


;; Layer 0
;; Set prefix key
(stumpwm:set-prefix-key (stumpwm:kbd "dead_grave"))


;; With modules
;; Pamixer
(define-key *top-map* (kbd "XF86AudioLowerVolume") "pamixer-volume-down")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "pamixer-volume-up")
(define-key *top-map* (kbd "XF86AudioMute") "pamixer-toggle-mute")

;; Brightness controls. You'll need to follow the acpi-backlight installation instructions to use these
(define-key *top-map* (kbd "XF86MonBrightnessUp") "backlight-up")
(define-key *top-map* (kbd "XF86MonBrightnessDown") "backlight-down")



;; Normal Screenshot
(define-key *top-map* (kbd "Print") "screenshot")


;; C-Layer 
;; Screenshot an area
(define-key *top-map* (kbd "C-Print") "screenshot-area")

;; H-Layer
;; Move between frames
(define-key *top-map* (kbd "H-i") "move-focus left")
(define-key *top-map* (kbd "H-\"") "move-focus up")
(define-key *top-map* (kbd "H-a") "move-focus right")
(define-key *top-map* (kbd "H-e") "move-focus down")
(define-key *top-map* (kbd "H-?") "sibling")

;; Split and unsplit frames
(define-key *top-map* (kbd "H-u") "vsplit")
(define-key *top-map* (kbd "H-,") "hsplit")
(define-key *top-map* (kbd "H-x") "remove-split")

;; Cycle programs within a frame
(define-key *top-map* (kbd "H-TAB") "next-in-frame")
(define-key *top-map* (kbd "H-ISO_Left_Tab") "prev-in-frame")

;; Open and save desktops
(define-key *top-map* (kbd "H-h") "restore-from-file")
(define-key *top-map* (kbd "H-s") "dump-desktop-to-file")

;; Other commands that don't require a module
(define-key *top-map* (kbd "H-f") "fullscreen")
(define-key *top-map* (kbd "H-k") "kill-window")
(define-key *top-map* (kbd "`") "send-raw-key")

;; Commands that depend on modules
;; Audio and brightness
(define-key *top-map* (kbd "H-q") "pamixer-volume-down")
(define-key *top-map* (kbd "H-v") "pamixer-volume-up")

(define-key *top-map* (kbd "H-.") "backlight-down")
(define-key *top-map* (kbd "H-b") "backlight-up")

;; Move between groups with Spatial-Groups
(define-key *top-map* (kbd "H-t") "coord-left")
(define-key *top-map* (kbd "H-l") "coord-up")
(define-key *top-map* (kbd "H-n") "coord-right")
(define-key *top-map* (kbd "H-r") "coord-down")
(define-key *top-map* (kbd "H-c") "coord-taskleft")
(define-key *top-map* (kbd "H-m") "coord-taskright")
(define-key *top-map* (kbd "H-g") "coord-taskorigin")
(define-key *top-map* (kbd "H-z") "coord-taskpop")
(define-key *top-map* (kbd "H-w") "groups")

;; Commands for the Winner-Mode module
(define-key *top-map* (kbd "H-o") "winner-undo")
(define-key *top-map* (kbd "H-d") "winner-redo")

;; Clipboard History
(define-key *top-map* (kbd "H-y") "show-clipboard-history")

;; Screenshot a window
(define-key *top-map* (kbd "H-Print") "screenshot-window")

;; DStump H-C-layer
;; Microphone audio settings
(define-key *top-map* (kbd "C-H-q") "pamixer-source-volume-down")
(define-key *top-map* (kbd "C-H-v") "pamixer-source-volume-up")

;; Split into specified sections
(define-key *top-map* (kbd "H-C-u") "hsplit-by-letter")
(define-key *top-map* (kbd "H-C-,") "vsplit-by-letter")

;; Move programs between frames
(define-key *top-map* (kbd "H-C-i") "move-window left")
(define-key *top-map* (kbd "H-C-\"") "move-window up")
(define-key *top-map* (kbd "H-C-a") "move-window right")
(define-key *top-map* (kbd "H-C-e") "move-window  down")

;; Move programs between desktops
(define-key *top-map* (kbd "H-C-t") "coord-move-left")
(define-key *top-map* (kbd "H-C-l") "coord-move-up")
(define-key *top-map* (kbd "H-C-n") "coord-move-right")
(define-key *top-map* (kbd "H-C-r") "coord-move-down")
(define-key *top-map* (kbd "H-C-c") "coord-move-taskleft")
(define-key *top-map* (kbd "H-C-m") "coord-move-taskright")


;; Keypad config
(define-key *top-map* (kbd "KP_0") "move-focus left") 
(define-key *top-map* (kbd "KP_2") "move-focus up")
(define-key *top-map* (kbd "KP_Enter") "move-focus right")
(define-key *top-map* (kbd "KP_Separator") "move-focus down")

;;With modules
(define-key *top-map* (kbd "KP_4") "coord-left")
(define-key *top-map* (kbd "KP_8") "coord-up")
(define-key *top-map* (kbd "KP_6") "coord-right")
(define-key *top-map* (kbd "KP_5") "coord-down")
(define-key *top-map* (kbd "KP_7") "coord-taskleft")
(define-key *top-map* (kbd "KP_9") "coord-taskright")


;; Root map
;; Activate Redshift by typing the letter corresponding to a strength level
(define-key *root-map* (kbd "i") "redshift-by-level")

;; Toggle Modeline
(define-key *root-map* (kbd "t") "mode-line")

;; Start Iresize
(define-key *root-map* (kbd "r") "iresize")

;; Check Battery
(define-key *root-map* (kbd ",")  "batterystate")

;; Reload Stump's config
(define-key *root-map* (kbd "c") "loadrc")

;; Switch to Run-App-Map and Emacs-Map
(define-key *root-map* (kbd "o") '*run-app-map*)
(define-key *root-map* (kbd "d") '*quit-map*)
(define-key *root-map* (kbd "e") '*emacs-map*)
(define-key *root-map* (kbd "h") '*help-map*)

;; With modules
;; Mute and unmute
(define-key *root-map* (kbd "q") "pamixer-mute")
(define-key *root-map* (kbd "v") "pamixer-unmute")

;; Lowest and Highest Brightness
(define-key *root-map* (kbd ".") "backlight-set 0")
(define-key *root-map* (kbd "b") "backlight-set 100")

;; H-layer
;; Mute and unmute microphone
(define-key *root-map* (kbd "H-q") "pamixer-source-mute")
(define-key *root-map* (kbd "H-v") "pamixer-source-unmute")

;; Exchange programs between frames
(define-key *root-map* (kbd "H-i") "exchange-direction left")
(define-key *root-map* (kbd "H-\"") "exchange-direction up")
(define-key *root-map* (kbd "H-a") "exchange-direction right")
(define-key *root-map* (kbd "H-e") "exchange-direction down")


;; Mouse Map
(binwarp:define-binwarp-mode binwarp-mode
  "H-space" (:map *top-map*
	     :redefine-bindings t)
  ((kbd "C-a") "binwarp left")
  ((kbd "C-e" ) "binwarp up")
  ((kbd "C-r") "binwarp down")
  ((kbd "C-t") "binwarp right")
  ((kbd "C-,") "ratclick 1")
  ((kbd "C-c") "ratclick 3")
  ((kbd "C-.") "ratclick 2")
  ((kbd "C-b") "ratclick 4")
  ((kbd "C-\"") "back-binwarp")
  
  ((kbd "KP_4") "binwarp left")
  ((kbd "KP_8") "binwarp up")
  ((kbd "KP_5") "binwarp down")
  ((kbd "KP_6") "binwarp right")
  ((kbd "KP_7") "ratclick 1")
  ((kbd "KP_9") "ratclick 3")
  )


;; Iresize Map
(define-interactive-keymap (iresize tile-group)
    (:on-enter #'setup-iresize
     :on-exit  #'resize-unhide
     :abort-if #'abort-resize-p
     :exit-on  ((kbd "RET") (kbd "ESC")
                            (kbd "C-g") (kbd "q")))
  
  ((kbd "a") "resize-direction left")
  ((kbd "r") "resize-direction down")
  ((kbd "e") "resize-direction up")
  ((kbd "t") "resize-direction right"))


;; Run-App-Map
(define-key *run-app-map* (kbd "f") "rr-firefox")


;; Quit-Map
(define-key *quit-map* (kbd "r") "restart-hard")
(define-key *quit-map* (kbd "s") "restart-soft")
(define-key *quit-map* (kbd "h") "exec /usr/bin/sudo pm-hibernate")
(define-key *quit-map* (kbd "k") "quit")
(define-key *quit-map* (kbd "w") "restartwireguard")


;; Emacs Map
(define-key *emacs-map* (kbd "E") "emacs")
(define-key *emacs-map* (kbd "d") "emacs-daemon-start")
(define-key *emacs-map* (kbd "q") "emacs-daemon-stop")
(define-key *emacs-map* (kbd "k") "emacs-daemon-kill-force")
(define-key *emacs-map* (kbd "e") "swm-emacs")


;; Remapped Keys
(define-remapped-keys
    `((,(lambda (win)
          (and (member (window-class win)
                       '("discord" "signal" "telegram" "zulip" "mplayer" "spotify" "qpdfview" "evince" "zotero" "calibre" "transmission")
                       :test #'string-equal)
               (not binwarp:*binwarp-mode-p*)))

        ;; Navigation
        ("C-a"   . "Left")
        ("C-t"   . "Right")

        ("C-e"   . "Up")
        ("C-r"   . "Down")

        ("C-q"   . "Prior")
        ("C-v"   . "Next")

        ("C-o"   . "Home")       
        ("C-d"   . "End")       

        ;; Undo and Redo
        ("C-\""   . "C-z")
        ("C-l"   . "C-y")

        ;; Copy, Cut and Paste
        ("C-w"   . "C-c")
        ("H-w"   . "C-x")
        ("C-y"   . "C-v")

        ;; Search
        ("C-n"   . "C-f")

        ;; Escape
        ("C-g"   . "Escape")

        ;; Open and Save
        ("C-h"   . "C-o")

        ;; Quit       
        ("C-!"   . "C-q")
        )
      
      
      (,(lambda (win)
          (and (member (window-class win)
                       '("Firefox")
                       :test #'string-equal)
               (not binwarp:*binwarp-mode-p*)))
        ("C-a"   . "Left")
        ("C-t"   . "Right")

        ("C-e"   . "Up")
        ("C-r"   . "Down")        

        ("C-o"   . "Home")
        ("C-d"   . "End")       

        ("C-q"   . "Prior")
        ("C-v"   . "Next")

        ("C-\""   . "C-z")
        ("C-l"   . "C-y")

        ("C-w"   . "C-c")
        ("H-w"   . "C-x")
        ("C-y"   . "C-v")

        ;; Search forward and backward
        ("C-n"   . "C-g")
        ("C-i"   . "C-S-g")

        ("C-h"   . "C-o")
        
        ("C-g"   . "Escape")

        ("C-b"   . "C-w")
        ("C-!"   . "C-q")
        )
      ))


;; Stuff not in Daselt
(defparameter *com-map* (make-sparse-keymap))

;; Wireguard keys
(define-key *root-map* (kbd "C-w") "nm-list-wireless-networks")
(define-key *root-map* (kbd "C-v") "nm-list-vpn-connections")

;; Further programs
(define-key *run-app-map* (kbd "x") "xterm")
(define-key *run-app-map* (kbd "X") "rr-xterm")
(define-key *run-app-map* (kbd "q") "rr-qpdfview")
(define-key *run-app-map* (kbd "s") "rr-spotify")
(define-key *run-app-map* (kbd "i") "rr-inkscape")
(define-key *run-app-map* (kbd "z") "rr-zotero")
(define-key *run-app-map* (kbd "C") "rr-calibre")
(define-key *run-app-map* (kbd "g") "rr-gimp")

;; Switch to Com-Map
(define-key *run-app-map* (kbd "c") '*com-map*)


;; Com-Map
(define-key *com-map* (kbd "b") "rr-protonmail-bridge")
(define-key *com-map* (kbd "s") "rr-signal")
(define-key *com-map* (kbd "t") "rr-telegram")
(define-key *com-map* (kbd "d") "rr-discord")
(define-key *com-map* (kbd "w") "rr-whatsie")
(define-key *com-map* (kbd "z") "rr-zoom")
(define-key *com-map* (kbd "Z") "rr-zulip")
