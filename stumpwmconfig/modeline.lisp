(in-package :stumpwm)

(setf *mode-line-screen-position* :bottom)
(setf *mode-line-frame-position* :bottom)
(setf *mode-line-border-width* 0)
(setf *mode-line-border-height* 0)
(setf *mode-line-pad-x* 0)
(setf *mode-line-pad-y* 1)
(setf *mode-line-background-color* "black")
(setf *mode-line-foreground-color* "grey")
(setf *mode-line-timeout* 2)
(setf *mode-line-border-color* "grey30")
(setf *window-format* "<%n%s%m%30t>")

;; (defun show-battery-charge ()
;;   (let ((raw-battery (run-shell-command "acpi | cut -d, -f2" t)))
;;     (substitute #\Space #\Newline raw-battery)))

;; (defun show-battery-state ()
;;   (let ((raw-battery (run-shell-command "acpi | cut -d: -f2 | cut -d, -f1" t)))
;;     (substitute #\Space #\Newline raw-battery)))

;; Switch mode-line on
;; (toggle-mode-line (current-screen) (current-head))

;; ;; Called from slime
;; (setf *JABBER-MODE-LINE* "")

;; Set model-line format
;; (setf *screen-mode-line-format*
;;       (list
;;        "Battery:"
;;        '(:eval (show-battery-charge))
;;        '(:eval (show-battery-state))
;;        "| %g"))




(setf stumpwm:*screen-mode-line-format*
      (list "%B | %I ^> %Q | "
            '(:eval (stumpwm:run-shell-command "date" t))))


