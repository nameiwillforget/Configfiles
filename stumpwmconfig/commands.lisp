(defcommand send-delete () ()
  (send-raw-key))

(defcommand select-window-by-letter (letter)
  ((:string "Letter: "))
  (progn
    (select-window-by-number) 
    (if (string= letter "t")
        (select-window-by-number 1))
    (if (string= letter "r")
        (select-window-by-number 2))
    (if (string= letter "n")
        (select-window-by-number 3))
    (if (string= letter "c")
        (select-window-by-number 4))
    (if (string= letter "l")
        (select-window-by-number 5))
    (if (string= letter "m")
        (select-window-by-number 6))
    (if (string= letter "g")
        (select-window-by-number 7))
    (if (string= letter "z")
        (select-window-by-number 8))
    (if (string= letter "w")
        (select-window-by-number 9))
    (if (string= letter " ")
        (select-window-by-number 10))
    )
  )

(defcommand hsplit-by-letter (letter)
  ((:string "Letter: "))
    (if (string= letter "t")
      (hsplit-equally 1))
  (if (string= letter "r")
      (hsplit-equally 2))
  (if (string= letter "n")
      (hsplit-equally 3))
  (if (string= letter "c")
      (hsplit-equally 4))
  (if (string= letter "l")
      (hsplit-equally 5))
  (if (string= letter "m")
      (hsplit-equally 6))
  (if (string= letter "g")
      (hsplit-equally 7))
  (if (string= letter "z")
      (hsplit-equally 8))
  (if (string= letter "w")
      (hsplit-equally 9))
  (if (string= letter " ")
      (hsplit-equally 10))
  )

(defcommand vsplit-by-letter (letter)
  ((:string "Letter: "))
    (if (string= letter "t")
      (vsplit-equally 1))
  (if (string= letter "r")
      (vsplit-equally 2))
  (if (string= letter "n")
      (vsplit-equally 3))
  (if (string= letter "c")
      (vsplit-equally 4))
  (if (string= letter "l")
      (vsplit-equally 5))
  (if (string= letter "m")
      (vsplit-equally 6))
  (if (string= letter "g")
      (vsplit-equally 7))
  (if (string= letter "z")
      (vsplit-equally 8))
  (if (string= letter "w")
      (vsplit-equally 9))
  (if (string= letter " ")
      (vsplit-equally 10))
  )


(defcommand redshift-by-level (level)
    ((:string "Level: "))
  (if (string= level "t")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 1000")))
  (if (string= level "r")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 1500")))
  (if (string= level "n")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 2000")))
  (if (string= level "c")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 2500")))
  (if (string= level "l")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 3000")))
  (if (string= level "m")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 3500")))
  (if (string= level "g")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 4000")))
  (if (string= level "z")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 5000")))
  (if (string= level "w")
      (progn (stumpwm:run-shell-command "redshift -x")
      (stumpwm:run-shell-command "redshift -O 5500")))
  (if (string= level " ")
      (stumpwm:run-shell-command "redshift -x"))
  )


;; command to grab a regional screenshot
(defcommand printscreenregion () ()
  (message "Print screen region")
  (run-shell-command "gnome-screenshot -a"))

;; command to launch or if already running give focus to spotify
(defcommand rr-spotify () ()
  (run-or-raise "spotify-launcher" '(:class "Spotify")))

;; command to launch or if already running give focus to firefox
;; cycles through multiple instances of firefox when repeated if
;; multiple instances exist
(defcommand rr-firefox () ()
  (run-or-raise "firefox" '(:class "Firefox")))


(defcommand xterm () ()
  (run-shell-command "xterm"))
;; command to launch or if already running give focus to xterm
;; cycles through multiple instances of xterm when repeated if
;; multiple instances exist

(defcommand rr-xterm () ()
  (run-or-raise "xterm" '(:class "XTerm")))

(defcommand rr-telegram () ()
  (run-or-raise "telegram-desktop" '(:class "telegram")))

(defcommand rr-signal () ()
  (run-or-raise "signal-desktop" '(:class "signal")))

(defcommand rr-protonmail-bridge () ()
  (run-or-raise "protonmail-bridge" '(:class "protonmail-bridge")))

(defcommand rr-discord () ()
  (run-or-raise "discord" '(:class "discord")))

(defcommand rr-zulip () ()
  (run-or-raise "zulip" '(:class "zulip")))

(defcommand rr-whatsie () ()
  (run-or-raise "whatsie" '(:class "whatsapp")))

(defcommand rr-zoom () ()
  (run-or-raise "zoom" '(:class "zoom")))

(defcommand rr-qpdfview () ()
  (run-or-raise "qpdfview" '(:class "qpdfview")))

(defcommand rr-inkscape () ()
  (run-or-raise "inkscape" '(:class "inkscape")))

(defcommand rr-gimp () ()
  (run-or-raise "gimp" '(:class "gimp")))

(defcommand rr-zotero () ()
  (run-or-raise "zotero" '(:class "zotero")))

(defcommand rr-calibre () ()
  (run-or-raise "calibre" '(:class "calibre")))


;; (defcommand redaselt () ()
;;   (run-shell-command "redaselt"))

(defcommand batterystate () ()
  (run-shell-command "acpi -b" T))

(defcommand restartwireguard () ()
  (run-shell-command "systemctl restart wg-quick@client"))


;; (defmacro fullscreen-in-frame-only-once ((win &key keep-until-matched) &body body)
;;   (with-gensyms (g l)
;;     `(define-fullscreen-in-frame-rule ,g (,win)
;;        (let ((,l (progn ,@body)))
;;          ,(if keep-until-matched
;;               `(when ,l (remove-fullscreen-in-frame-rule ',g))
;;               `(remove-fullscreen-in-frame-rule ',g))
;;          ,l))))




;; ;; command to send the content of the x clipboard to the
;; ;; currently focused window
;; (defcommand cheatypaste () ()
;;   (window-send-string
;;     (get-x-selection)
;;     (screen-current-window (current-screen))))

;; ;; stumpwm doesn't handle us-intl-altgr keyboard properly
;; ;; the below adds similar keyboard shortcuts for inserting
;; ;; nordic characters in stumpwm inputs using the super key
;; ;; instead of alt-gr
;; (define-key *input-map* (kbd "s-w")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Ã¥)))
;; (define-key *input-map* (kbd "s-W")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Ã…)))
;; (define-key *input-map* (kbd "s-l")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Ã¸)))
;; (define-key *input-map* (kbd "s-L")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Ã˜)))
;; (define-key *input-map* (kbd "s-z")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Ã¦)))
;; (define-key *input-map* (kbd "s-Z")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Ã†)))
;; (define-key *input-map* (kbd "s-[")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Â«)))
;; (define-key *input-map* (kbd "s-]")
;;   #'(lambda (input c)
;;       (declare (ignore c))
;;       (input-insert-char input #\Â»)))


;; (defcommand battery () ()
;;   (message 
;;    (run-shell-command "acpi -b -a -t" t)))

