;;; customizations.el --- This file houses the customizations defined through Emacs' interface.  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Alexander Prähauser

;; Author: Alexander Prähauser <alexander.praehauser@gmx.at>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-insert-braces nil)
 '(auto-insert-alist
   '((("\\.\\([Hh]\\|hh\\|hpp\\|hxx\\|h\\+\\+\\)\\'" . "C / C++ header")
      (replace-regexp-in-string "[^A-Z0-9]" "_"
                                (string-replace "+" "P"
                                                (upcase
                                                 (file-name-nondirectory buffer-file-name))))
      "#ifndef " str n "#define " str "\12\12" _ "\12\12#endif")
     (("\\.\\([Cc]\\|cc\\|cpp\\|cxx\\|c\\+\\+\\)\\'" . "C / C++ program") nil
      "#include \""
      (let ((stem (file-name-sans-extension buffer-file-name)) ret)
        (dolist (ext '("H" "h" "hh" "hpp" "hxx" "h++") ret)
          (when (file-exists-p (concat stem "." ext))
            (setq ret (file-name-nondirectory (concat stem "." ext))))))
      & 34 | -10)
     (("[Mm]akefile\\'" . "Makefile") . "makefile.inc")
     (html-mode . #[0 "\300\301!\207" [sgml-tag "html"] 2])
     (plain-tex-mode . "tex-insert.tex") (bibtex-mode . "tex-insert.tex")
     (latex-mode "options, RET: " "\\documentclass[" str & 93 | -1 123
                 (read-string "class: ") "}\12"
                 ("package, %s: " "\\usepackage[" (read-string "options, RET: ") & 93 | -1
                  123 str "}\12")
                 "\12\\title{" (read-string "Title, RET: ") "}\12"
                 "\12\\author{Alexander Prähauser}\12\\date{\\today}\12"
                 "\12\\begin{document}\12\\maketitle\12%\\tableofcontents\12\12" _
                 "\12%\\section{Bibliography}\12%label{sec:bibliography}\12\12%heading=bibintoc,\12%title={Bibliography}\12%]\12\12\\end{document}")
     (("/bin/.*[^/]\\'" . "Shell-Script mode magic number") .
      #[0 "\10\301\300!=\205\12\0\302 \207" [major-mode default-value sh-mode] 3])
     (ada-mode . ada-skel-initial-string)
     (("\\.[1-9]\\'" . "Man page skeleton") "Short description: " ".\\\" Copyright (C), "
      (format-time-string "%Y") "  " (getenv "ORGANIZATION") | (progn user-full-name)
      "\12.\\\" You may distribute this file under the terms of the GNU Free\12.\\\" Documentation License.\12.TH "
      (file-name-base (buffer-file-name)) " " (file-name-extension (buffer-file-name)) " "
      (format-time-string "%Y-%m-%d ") "\12.SH NAME\12"
      (file-name-base (buffer-file-name)) " \\- " str "\12.SH SYNOPSIS\12.B "
      (file-name-base (buffer-file-name)) "\12" _
      "\12.SH DESCRIPTION\12.SH OPTIONS\12.SH FILES\12.SH \"SEE ALSO\"\12.SH BUGS\12.SH AUTHOR\12"
      (user-full-name)
      '(if (search-backward "&" (line-beginning-position) t)
           (replace-match (capitalize (user-login-name)) t t))
      '(end-of-line 1) " <" (progn user-mail-address) ">\12")
     (".dir-locals.el" nil
      ";;; Directory Local Variables         -*- no-byte-compile: t; -*-\12"
      ";;; For more information see (info \"(emacs) Directory Variables\")\12\12" "(("
      '(setq v1
             (let (modes)
               (mapatoms
                (lambda (mode)
                  (let ((name (symbol-name mode)))
                    (when (string-match "-mode\\'" name) (push name modes)))))
               (sort modes 'string<)))
      (completing-read "Local variables for mode: " v1 nil t) " . (("
      (let
          ((all-variables
            (apropos-internal ".*"
                              (lambda (symbol)
                                (and (boundp symbol) (get symbol 'variable-documentation))))))
        (completing-read "Variable to set: " all-variables))
      " . " (completing-read "Value to set it to: " nil) "))))\12")
     (("\\.el\\'" . "Emacs Lisp header") "Short description: " ";;; "
      (file-name-nondirectory (buffer-file-name)) " --- " str
      (make-string (max 2 (- 80 (current-column) 27)) 32) "-*- lexical-binding: t; -*-"
      '(setq lexical-binding t) "\12\12;; Copyright (C) " (format-time-string "%Y") "  "
      (getenv "ORGANIZATION") | (progn user-full-name) "\12\12;; Author: "
      (user-full-name)
      '(if (search-backward "&" (line-beginning-position) t)
           (replace-match (capitalize (user-login-name)) t t))
      '(end-of-line 1) " <" (progn user-mail-address) ">\12;; Keywords: "
      '(require 'finder)
      '(setq v1 (mapcar (lambda (x) (list (symbol-name (car x)))) finder-known-keywords)
             v2
             (mapconcat (lambda (x) (format "%12s:  %s" (car x) (cdr x)))
                        finder-known-keywords "\12"))
      ((let ((minibuffer-help-form v2)) (completing-read "Keyword, C-h: " v1 nil t)) str
       ", ")
      & -2
      "\12\12;; This program is free software; you can redistribute it and/or modify\12;; it under the terms of the GNU General Public License as published by\12;; the Free Software Foundation, either version 3 of the License, or\12;; (at your option) any later version.\12\12;; This program is distributed in the hope that it will be useful,\12;; but WITHOUT ANY WARRANTY; without even the implied warranty of\12;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\12;; GNU General Public License for more details.\12\12;; You should have received a copy of the GNU General Public License\12;; along with this program.  If not, see <https://www.gnu.org/licenses/>.\12\12;;; Commentary:\12\12;; "
      _ "\12\12;;; Code:\12\12\12\12(provide '" (file-name-base (buffer-file-name))
      ")\12;;; " (file-name-nondirectory (buffer-file-name)) " ends here\12")
     (("\\.texi\\(nfo\\)?\\'" . "Texinfo file skeleton") "Title: "
      "\\input texinfo   @c -*-texinfo-*-\12@c %**start of header\12@setfilename "
      (file-name-base (buffer-file-name)) ".info\12" "@settitle " str
      "\12@c %**end of header\12@copying\12"
      (setq short-description (read-string "Short description: ")) ".\12\12"
      "Copyright @copyright{} " (format-time-string "%Y") "  " (getenv "ORGANIZATION") |
      (progn user-full-name)
      "\12\12@quotation\12Permission is granted to copy, distribute and/or modify this document\12under the terms of the GNU Free Documentation License, Version 1.3\12or any later version published by the Free Software Foundation;\12with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.\12A copy of the license is included in the section entitled ``GNU\12Free Documentation License''.\12\12A copy of the license is also available from the Free Software\12Foundation Web site at @url{https://www.gnu.org/licenses/fdl.html}.\12\12@end quotation\12\12The document was typeset with\12@uref{https://www.gnu.org/software/texinfo/, GNU Texinfo}.\12\12@end copying\12\12@titlepage\12@title "
      str "\12@subtitle " short-description "\12@author " (getenv "ORGANIZATION") |
      (progn user-full-name) " <" (progn user-mail-address)
      ">\12@page\12@vskip 0pt plus 1filll\12@insertcopying\12@end titlepage\12\12@c Output the table of the contents at the beginning.\12@contents\12\12@ifnottex\12@node Top\12@top "
      str
      "\12\12@insertcopying\12@end ifnottex\12\12@c Generate the nodes for this menu with `C-c C-u C-m'.\12@menu\12@end menu\12\12@c Update all node entries with `C-c C-u C-n'.\12@c Insert new nodes with `C-c C-c n'.\12@node Chapter One\12@chapter Chapter One\12\12"
      _
      "\12\12@node Copying This Manual\12@appendix Copying This Manual\12\12@menu\12* GNU Free Documentation License::  License for copying this manual.\12@end menu\12\12@c Get fdl.texi from https://www.gnu.org/licenses/fdl.html\12@include fdl.texi\12\12@node Index\12@unnumbered Index\12\12@printindex cp\12\12@bye\12")))
 '(avy-timeout-seconds 0.5)
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(browse-url-browser-function 'eww-browse-url)
 '(cdlatex-math-symbol-prefix 1083)
 '(custom-enabled-themes '(sanityinc-tomorrow-bright))
 '(custom-safe-themes
   '("b11edd2e0f97a0a7d5e66a9b82091b44431401ac394478beb44389cf54e6db28"
     "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" default))
 '(eww-search-prefix "https://www.startpage.com/sp/search?query=")
 '(fill-column 90)
 '(font-latex-math-environments
   '("display" "displaymath" "equation" "eqnarray" "gather" "multline" "align" "alignat"
     "xalignat" "dmath" "tikzcd"))
 '(image-animate-loop t)
 '(inhibit-startup-screen t)
 '(max-lisp-eval-depth 16000)
 '(org-directory "~/.emacs.d/org")
 '(org-image-actual-width 900)
 '(org-latex-packages-alist '(("" "DLua" t nil)))
 '(org-link-context-for-files 1)
 '(org-roam-capture-templates
   '(("N" "Org-noter note file template" plain "%?" :target
      (file "~/.emacs.d/org-roam/org-noter/%<%Y%m%d%H%M%S>-${slug}.org") nil nil)
     ("ad" "German article template" plain
      (file "~/.emacs.d/org-roam/templates/article.org") :target
      (file+head "~/.emacs.d/org-roam/Articles/Deutsch/%<%Y%m%d%H%M%S>-${slug}.de"
                 "#+title: ${title}")
      nil nil)
     ("ae" "English article template" plain (file "~/org-roam/templates/article.org")
      :target
      (file+head "~/.emacs.d/org-roam/Articles/English/%<%Y%m%d%H%M%S>-${slug}.org"
                 "#+title: ${title}")
      nil nil)
     ("d" "Definition template" plain
      (file "~/.emacs.d/org-roam/templates/definition.org") :target
      (file+head "~/.emacs.d/org-roam/Math/%<%Y%m%d%H%M%S>-${slug}.org"
                 "#+title: ${title}")
      :immediate-finish t nil nil)
     ("t" "Theorem template" plain (file "~/.emacs.d/org-roam/templates/theorem.org")
      :target
      (file+head "~/.emacs.d/org-roam/Math/%<%Y%m%d%H%M%S>-${slug}.org"
                 "#+title: ${title}")
      :immediate-finish t :unnarrowed t nil nil)
     ("D" "default" plain "%?" :target
      (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\12") :unnarrowed t)))
 '(org-roam-directory "~/.emacs.d/org-roam")
 '(org-structure-template-alist
   '(("a" . "export ascii") ("c" . "center") ("C" . "comment") ("e" . "example")
     ("E" . "export") ("h" . "export html") ("l" . "export latex") ("q" . "quote")
     ("s" . "src") ("v" . "verse") ("{" . "src emacs-lisp")))
 '(package-selected-packages
   '(DEmacs ace-flyspell ace-isearch ace-jump-mode aggressive-indent all
            all-the-icons-completion all-the-icons-dired all-the-icons-gnus
            all-the-icons-ibuffer asx auctex-cluttex auto-dictionary auto-yasnippet
            back-button backup-walker backward-forward bookmark+ buffer-move bug-hunter
            cape cdlatex citar-embark citar-org-roam color-theme-sanityinc-tomorrow
            consult-org-roam corfu-prescient corfu-terminal crontab-mode dedicated
            default-text-scale deft diminish dired-du dired-quick-sort dired-toggle-sudo
            dream-theme easy-kill-extras edit-server elgrep embark-consult emms
            expand-region fancy-compilation flyspell-correct-helm flyspell-lazy forge
            free-keys gnuplot god-mode gotham-theme helm-bibtex helm-bufler helm-descbinds
            helm-dictionary helm-dired-history helm-flyspell helm-icons helm-swoop
            helm-wikipedia help-find-org-mode isearch+ isearch-prop kind-icon latex-extra
            marginalia mood-line mu4easy orderless org-modern org-noter-pdftools
            org-projectile-helm org-ref org-roam-bibtex org-roam-ui org-super-links
            paredit-everywhere persp-projectile quelpa-use-package rainbow-delimiters
            smart-mark smartparens smudge speed-type spray tabgo telega
            treemacs-all-the-icons treemacs-icons-dired treemacs-magit treemacs-persp
            treemacs-projectile treemacs-tab-bar try undo-tree unfill unicode-fonts
            use-package-ensure-system-package vertico-prescient visible-mark vterm-toggle
            w3m wakib-keys wc-mode workgroups2 yaml-mode zotero ztree))
 '(reftex-ref-style-alist
   '(("Default" t (("\\ref" 13) ("\\Ref" 82) ("\\footref" 110) ("\\pageref" 112)))
     ("Varioref" "varioref" (("\\vref" 118) ("\\Vref" 86) ("\\vpageref" 103)))
     ("Fancyref" "fancyref" (("\\fref" 102) ("\\Fref" 70)))
     ("Hyperref" "hyperref" (("\\autoref" 97) ("\\autopageref" 117)))
     ("Cleveref" t (("\\cref" 99) ("\\Cref" 67) ("\\cpageref" 100) ("\\Cpageref" 68)))
     ("AMSmath" "amsmath" (("\\eqref" 101)))))
 '(safe-local-variable-values
   '((org-roam-db-location . "~/.emacs.d/org-roam/Math/org-roam.db")
     (org-roam-directory . "~/.emacs.d/org-roam/Math")
     (org-roam-db-location . "~/.emacs.d/org-roam/Articles/org-roam.db")
     (org-roam-directory . "~/.emacs.d/org-roam/Articles")
     (org-roam-db-location . "~/.emacs.d/org-roam/Wissenschaft der Logik/org-roam.db")
     (org-roam-directory . "~/.emacs.d/org-roam/Wissenschaft der Logik")
     (org-roam-db-location . "~/org-roam/Articles/org-roam.db")
     (org-roam-directory . "~/org-roam/Articles")
     (eval and buffer-file-name (not (eq major-mode 'package-recipe-mode))
           (or (require 'package-recipe-mode nil t)
               (let ((load-path (cons "../package-build" load-path)))
                 (require 'package-recipe-mode nil t)))
           (package-recipe-mode))
     (org-roam-db-location . "~/org-roam/Artikel/org-roam.db")
     (org-roam-directory . "~/org-roam/Artikel") (mangle-whitespace . t)
     (org-roam-db-location . "~/org-roam/Math/org-roam.db")
     (org-roam-directory . "~/org-roam/Math")
     (org-roam-db-location . "~/org-roam/Wissenschaft der Logik/org-roam.db")
     (org-roam-directory . "~/org-roam/Wissenschaft der Logik")))
 '(sage-shell:use-prompt-toolkit nil)
 '(sage-shell:use-simple-prompt t)
 '(search-invisible t)
 '(tex-start-options "--shell-escape")
 '(warning-suppress-types '((auto-save))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
