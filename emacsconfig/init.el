(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-insert-braces nil)
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(custom-safe-themes t
                      ;; '("1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e"
                      ;;   default)
                      )
 '(custom-enabled-themes '(sanityinc-tomorrow-bright))
 '(font-latex-math-environments
   '("display" "displaymath" "equation" "eqnarray" "gather" "multline"
     "align" "alignat" "xalignat" "dmath" "tikzcd"))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(ac-helm ac-math ace-flyspell ace-isearch ace-jump-mode
             aggressive-indent all-the-icons-completion
             all-the-icons-dired all-the-icons-gnus
             all-the-icons-ibuffer asx auctex-cluttex
             auto-complete-auctex auto-complete-sage auto-dictionary
             auto-yasnippet back-button backup-walker backward-forward
             bookmark+ cape cdlatex centaur-tabs citar-embark
             color-theme-sanityinc-tomorrow corfu-prescient
             corfu-terminal dedicated default-text-scale diminish
             dired-du dired-quick-sort dired-toggle-sudo dream-theme
             easy-kill-extras edit-server elgrep embark-consult emms
             expand-region fancy-compilation flyspell-correct-helm
             flyspell-lazy forge free-keys gnuplot god-mode
             gotham-theme helm-bibtex helm-bufler helm-descbinds
             helm-dictionary helm-dired-history helm-flyspell
             helm-icons helm-swoop helm-wikipedia help-find-org-mode
             isearch+ isearch-prop kind-icon marginalia mood-line
             mu4easy orderless org-modern org-noter-pdftools
             org-projectile-helm org-ref org-roam-bibtex
             paredit-everywhere persp-projectile quelpa-use-package
             rainbow-delimiters slime-company smart-mark smartparens
             smudge speed-type spray telega treemacs-all-the-icons
             treemacs-icons-dired treemacs-magit treemacs-persp
             treemacs-projectile treemacs-tab-bar try undo-tree unfill
             unicode-fonts use-package-ensure-system-package
             vertico-prescient visible-mark vterm-toggle w3m
             wakib-keys wc-mode workgroups2 yaml-mode
             yasnippet-snippets zotero))
 '(sage-shell:use-prompt-toolkit nil)
 '(sage-shell:use-simple-prompt t)
 '(tex-start-options "--shell-escape")
 '(warning-suppress-types '((auto-save))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )



(setq custom-file "~/.emacs.d/emacsconfig/customizations.el")
(load custom-file)


(setq user-init-dir "~/.emacs.d/emacsconfig/")
(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(setq user-private-dir "~/.emacs.d/emacsconfig/private/")
(defun load-private-file (file)
  (interactive "f")
  "Load a file in current user's private configuration directory"
  (load-file (expand-file-name file user-private-dir)))


(load-user-file "settings.el")
;; (load-user-file "add-tex-envs.el")
(load-user-file "commands.el")
(load-user-file "packages.el")
(load-user-file "keys.el")
(load-user-file "modes.el")
(load-user-file "org.el")

;; (load-private-file "mu4e.el")
(load-private-file "gnus.el")


;; For some reason, this doesn't work on my desktop when I put it into settings.el, so I put it here
(menu-bar-mode -1)


(put 'dired-find-alternate-file 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'LaTeX-narrow-to-environment 'disabled nil)
(put 'TeX-narrow-to-group 'disabled nil)
