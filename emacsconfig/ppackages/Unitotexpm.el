 ;;; Unitotexpm.el --- a simple package

;; Copyright (C) 2023 FSF

;; Author: Alexander Prähauser <ahprae@protonmail.com>
;; Keywords: latex unicode
;; Version: 0.3

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary: This is a simple package that can replace latex-symbols in a file with the corresponding unicode-symbols and vice versa. It defines two commands, unitotex and textouni, which do exactly what you'd expect. It also defines some global variables housing lists of pairs of tex and unicode symbols. The current range of symbols is about the range of symbols I can write on my keyboard. If you need different symbols you can write a list tuples of like those below and add it to ahpmath. You can also send it to me so I'll add it to the package.


;;; Code:

;; code goes here

(provide 'test)
;;; test.el ends here

(defconst lowgreek (list (list '"\\\\α" '"α") (list '"\\\\beta" '"β") (list '"\\\\gamma" '"γ") (list '"\\\\delta" '"δ") (list '"\\\\epsilon" '"ε")
 (list '"\\\\zeta" '"ζ") (list '"\\\\eta" '"η") (list '"\\\\theta" '"θ") (list '"\\\\iota" '"ι") (list '"\\\\kappa" '"κ") (list '"\\\\lambda" '"λ")
 (list '"\\\\mu" '"μ") (list '"\\\\nu" '"ν") (list '"\\\\xi" '"ξ") (list '"\\\\pi" '"π") (list '"\\\\rho" '"ρ") (list '"\\\\sigma" '"σ")
 (list '"\\\\varsigma" '"ς") (list '"\\\\tau" '"τ") (list '"\\\\upsilon" '"υ") (list '"\\\\phi" '"φ") (list '"\\\\chi" '"χ") (list '"\\\\psi" '"ψ")
 (list '"\\\\omega" '"ω") (list '"\\\\heta" '"ͱ")))


(defconst upgreek (list (list '"\\\\Gamma" '"Γ") (list '"\\\\Delta" '"Δ") (list '"\\\\Theta" '"Θ") (list '"\\\\Lambda" '"Λ") (list '"\\\\Xi" '"Ξ") (list '"\\\\Pi" '"Π")
 (list '"\\\\Sigma" '"Σ") (list '"\\\\Phi" '"Φ") (list '"\\\\Psi" '"Ψ") (list '"\\\\Omega" '"Ω") (list '"\\\\Heta" '"Ͱ")))


(defconst lowcyrsym (list (list '"\\\\mttoa{\\\\cyrb}" '"б") (list '"\\\\mttoa{\\\\cyrg}" '"г") (list '"\\\\mttoa{\\\\cyrd}" '"д") (list '"\\\\mttoa{\\\\cyrzh}" '"ж") (list '"\\\\mttoa{\\\\cyrz}" '"э") (list '"\\\\mttoa{\\\\cyri}" '"и")
 (list '"\\\\mttoa{\\\\cyr}" '"к") (list '"\\\\mttoa{\\\\cyrl}" '"л") (list '"\\\\mttoa{\\\\cyrm}" '"м") (list '"\\\\mttoa{\\\\cyrn}" '"н") (list '"\\\\mttoa{\\\\cyro}" '"о") (list '"\\\\mttoa{\\\\cyrp}" '"п")
 (list '"\\\\mttoa{\\\\cyrr}" '"р") (list '"\\\\mttoa{\\\\cyrs}" '"с") (list '"\\\\mttoa{\\\\cyrt}" '"т") (list '"\\\\mttoa{\\\\cyru}" '"у") (list '"\\\\mttoa{\\\\cyrf}" '"ф") (list '"\\\\mttoa{\\\\cyrh}" '"х")
 (list '"\\\\mttoa{\\\\cyrc}" '"ц") (list '"\\\\mttoa{\\\\cyrch}" '"ч") (list '"\\\\mttoa{\\\\cyrsh}" '"ш") (list '"\\\\mttoa{\\\\cyrshch}" '"щ") (list '"\\\\mttoa{\\\\cyrhrdsn}" '"ъ")
 (list '"\\\\mttoa{\\\\cyrery}" '"ы") (list '"\\\\mttoa{\\\\cyrsftsn}" '"ь") (list '"\\\\mttoa{\\\\cyryu}" '"ю") (list '"\\\\mttoa{\\\\cyrya}" '"я") (list '"\\\\mttoa{\\\\cyrdje}" '"ђ")
 (list '"\\\\mttoa{\\\\cyrnje}" '"њ") (list '"\\\\mttoa{\\\\cyrdzhe}" '"џ")))
 

(defconst mathl4 (list (list '"\\\\mathbb{E}" '"𝔼") (list '"\\\\exists" '"∃") (list '"\\\\mathbb{Q}" '"ℚ") (list '"\\\\mathbb{V}" '"𝕍")
 (list '"\\\\mathbb{C}" '"ℂ") (list '"\\\\mathbb{M}" '"𝕄") (list '"\\\\mathbb{J}" '"𝕁") (list '"\\\\mathbb{H}" '"ℍ")
 (list '"\\\\mathbb{I}" '"𝕀") (list '"\\\\forall" '"∀") (list '"\\\\aleph" '"ℵ") (list '"\\\\grad" '"∇")
 (list '"\\\\mathbb{T}" '"𝕋") (list '"\\\\mathbb{R}" '"ℝ") (list '"\\\\mathbb{N}" '"ℕ")
 (list '"\\\\mathbb{K}" '"𝕂") (list '"\\\\mathbb{Y}" '"𝕐") (list '"\\\\beth" '"ב") (list '"\\\\mathbb{Z}" '"ℤ")))


;; (defconst mathl4 (list (list '"$\\\\mathbb{E}$" '"𝔼") (list '"\\\\exists" '"∃") (list '"$\\\\mathbb{Q}$" '"ℚ") (list '"$\\\\mathbb{V}$" '"𝕍")
;;  (list '"$\\\\mathbb{C}$" '"ℂ") (list '"$\\\\mathbb{M}$" '"𝕄") (list '"\\\\mathbb{J}" '"𝕁") (list '"$\\\\mathbb{H}$" '"ℍ")
;;  (list '"$\\\\mathbb{I}$" '"𝕀") (list '"\\\\forall" '"∀") (list '"\\\\aleph" '"ℵ") (list '"\\\\grad" '"∇")
;;  (list '"$\\\\mathbb{T}$" '"𝕋") (list '"$\\\\mathbb{R}$" '"ℝ") (list '"$\\\\mathbb{N}$" '"ℕ")
;;  (list '"$\\\\mathbb{K}$" '"𝕂") (list '"$\\\\mathbb{Y}$" '"𝕐") (list '"\\\\beth" '"ב") (list '"$\\\\mathbb{Z}$" '"ℤ")))

  
(defconst mathl6 (list (list '"\\\\times" '"×") (list '"\\\\nwarrow" '"↖") (list '"\\\\uparrow" '"↑") (list '"\\\\nearrow↗" '"↗") (list '"\\\\ltimes" '"⋉")
 (list '"\\\\rtimes" '"⋊") (list '"\\\\prec" '"≺") (list '"\\\\wedge" '"∧") (list '"\\\\succ" '"≻") (list '"\\\\prod" '"∏")
 (list '"\\\\coprod" '"∐") (list '"\\\\leftrightarrow" '"↔") (list '"\\\\leftarrow" '"←") (list '"\\\\downarrow" '"↓")
 (list '"\\\\rightarrow" '"→") (list '"\\\\mapsto" '"↦") (list '"\\\\partial" '"∂") (list '"\\\\vee" '"∨") (list '"\\\\circ" '"∘")
 (list '"\\\\sum" '"∑") (list '"\\\\swarrow" '"↙") (list '"\\\\diamond" '"⋄") (list '"\\\\varnothing" '"∅") (list '"\\\\searrow" '"↘")
 (list '"\\\\in " '"∈ ") (list '"\\\\infty" '"∞") (list '"\\\\sqrt" '"√") (list '"\\\\parallel" '"∥") (list '"\\\\int" '"∫")))


(defconst ahptipa (list (list '"\\\\msci" '"ɪ") (list '"\\\\mce" '"ɞ") (list '"\\\\mthorn" '"þ") (list '"\\\\mdh" '"
ð") (list '"\\\\mglotstop" '"ʔ") (list '"\\\\doublebarpipe" '"ǂ")))

(defconst ahpdiacritics (list (list '"\\\\check" '"ˇ")))


(defconst ahpmath (append lowgreek upgreek lowcyrsym mathl4 mathl6 ahptipa ahpdiacritics))


(defun textouni ()
  "Replace tex-macros with Unicode characters"
  (interactive)
  (cl-mapcar (lambda (x) (progn (mark-whole-buffer) (replace-regexp (nth 0 x) (nth 1 x)) (princ (nth 0 x)))) ahpmath))

(defun unitotex ()
  "Replace tex-macros with Unicode characters"
  (interactive)
  (cl-mapcar (lambda (x) (progn (mark-whole-buffer) (replace-regexp (nth 1 x) (nth 0 x)) (princ (nth 0 x)))) ahpmath))
 
