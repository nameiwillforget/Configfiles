;; Translations of keys
;; Bind C-m to something less inocuous than RET. This was taken from here: https://emacs.stackexchange.com/a/79130/39786
(key-translate "C-m" "C-á")
(key-translate "C-i" "C-ĥ")

;; Transfer all keybindings on C-c to M-c. 
(define-key global-map (kbd "C-χ") (rebinder-dynamic-binding "C-c"))
(define-key global-map (kbd "C-∀") (rebinder-dynamic-binding "C-h"))
(rebinder-hook-to-mode 't 'after-change-major-mode-hook)

;; Set modifier applicators:
(global-unset-key (kbd "<XF86Launch6>"))
(global-unset-key (kbd "<kp-8>"))
(global-unset-key (kbd "<kp-9>"))
(global-unset-key (kbd "<kp-multiply>"))
(global-unset-key (kbd "<kp-divide>"))
(global-unset-key (kbd "`"))

(define-key function-key-map (kbd "<XF86Launch6>") 'event-apply-meta-modifier)
(define-key function-key-map (kbd "<kp-8>") 'event-apply-control-modifier)
(define-key function-key-map (kbd "<kp-9>") 'event-apply-meta-modifier)
(define-key function-key-map (kbd "<kp-multiply>") 'event-apply-hyper-modifier)
(define-key function-key-map (kbd "<kp-divide>") 'event-apply-hyper-modifier)
(define-key function-key-map (kbd "`") 'event-apply-super-modifier)

(define-key function-key-map (kbd "M-`") 'MetaSuper-next-cmd)
(bind-key* "M-`" 'MetaSuper-next-cmd)
(bind-key* "M-s-t" 'backward-word)
;; (define-key global-map (kbd "C-ω") 'kill-region)
;; (define-key global-map (kbd "C-Ω") 'save-to-register)

(bind-key "M-p" 'Daselt-mode global-map)
(bind-key "M-C-p" #'Daselt-basic-mode global-map)

;; Make s-` act like `
(bind-key* "s-`"   (lambda () (interactive) (insert "`")))

;; Commands.
;; ahpfwd will complete Corfu/Vertico completions if they are available and move forward otherwise, ahpbwd will quit corfu completions if they are available and move backward otherwise.
(bind-key* "C-a" 'ahpbwd)
(bind-key* "C-t" 'ahpfwd)

(bind-key* "C-<right>" 'backward-word)
(bind-key* "C-1" 'forward-word)

(bind-key* "C-e" 'ahpup)
(bind-key* "C-r" 'ahpdown)

;; Some modes, like Corfu, rebind previous-line and next-line, which doesn't work if they are defined in a separate function. For this reason we can't use bind-key* with a custom function excluding specific modes.
(bind-key "C-e" 'previous-line global-map)
(bind-key "C-r" 'next-line global-map)

(bind-key* "C-<down>" 'backward-paragraph)
(bind-key* "C-2" 'forward-paragraph)

(bind-key* "C-o" 'ahphome)
(bind-key* "C-d" 'ahpend)

(bind-key* "C-<next>" 'backward-sentence)
(bind-key* "C-<kp-enter>" 'forward-sentence)

(bind-key* "C-q" 'ahpprevious)
(bind-key* "C-v" 'ahpnext)

(bind-key* "C-<end>" 'beginning-of-buffer)
(bind-key* "C-<kp-subtract>" 'end-of-buffer)

(bind-key* "C-ℚ" 'back-button-local-backward)
(bind-key* "C-𝕍" 'back-button-local-forward)

(bind-key* "M-." 'icicle-goto-marker)

(bind-key* "C-<XF86Launch6>" 'recenter-top-bottom)


;;Sexp-navigation
(bind-key* "C-(" 'sp-backward-sexp)
(bind-key* "C-$" 'sp-forward-sexp)

(bind-key* "C-{" 'backward-up-list)
(bind-key* "C-*" 'down-list)
(bind-key* "C-S-*" 'forward-up-list)

(bind-key* "C-÷" 'beginning-of-defun)
(bind-key* "C-×" 'end-of-defun)


;; Sexp-marking
(bind-key* "M-d" 'sp-forward-slurp-sexp)
(bind-key* "M-C-d" 'sp-forward-barf-sexp)

(bind-key* "M-o" 'sp-backward-slurp-sexp)
(bind-key* "M-C-d" 'sp-backward-barf-sexp)

(bind-key* "M-(" 'sp-wrap-round)
(bind-key* "M-{" 'sp-wrap-curly)
(bind-key* "M-[" 'sp-wrap-square)

(bind-key* "M-$" 'sp-unwrap-sexp)
(bind-key* "M-*" 'sp-backward-unwrap-sexp)

(bind-key* "M-¬" 'sp-splice-sexp)
(bind-key* "M-C-¬" 'sp-splice-killing-around)

(bind-key* "M-∇" 'sp-split-sexp)
(bind-key* "M-Δ" 'sp-join-sexp)

(bind-key* "M-<kp-prior>" 'sp-convolute-sexp)


;; Some marking commands
(bind-key* "M-h" 'mark-defun)
(bind-key* "M-s" 'mark-paragraph)
(bind-key* "C-M-h" 'easy-mark-sexp)
(bind-key* "C-," 'easy-mark)
(bind-key* "M-," 'set-mark-command)
(bind-key* "M-@" 'exchange-point-and-mark)

;; Re-configure killing
(bind-key* [remap kill-ring-save] 'easy-kill)
(bind-key* "C-w" 'easy-kill 'global-map)
(bind-key* "C-ω" 'kill-region)

(bind-key* "s-k" 'kill-whole-line)

;; Search commands
(bind-key* "C-ĥ" 'isearch-backward)
(define-key isearch-mode-map (kbd "C-ĥ") 'isearch-repeat-backward)

(bind-key* "C-/" 'isearch-backward-regexp)

(bind-key* "C-n" 'isearch-forward)
(define-key isearch-mode-map (kbd "C-n") 'isearch-repeat-forward)

(bind-key* "C-\\" 'isearch-forward-regexp)


;;Replace Commands
(bind-key* "M-n" 'replace-string)
(bind-key* "M-\\" 'replace-regexp)

(bind-key* "M-∫" 'query-replace)
(bind-key* "C-M-∫" 'query-replace-regexp)

(bind-key* "M-↔" 'ahp/exchange)


;; Upcasing and downcasing
(bind-key* "M-∧" 'upcase-region)
(bind-key* "M-∨" 'downcase-region)


;; Commenting
(bind-key* "s-x" 'comment-line)

;; Opening and Saving
(bind-key* (kbd "C-h") 'find-file)
(bind-key* "C-s" 'save-buffer)

;; Closing
(bind-key* "C-s-k" 'save-buffers-kill-emacs)

;; Browsing
(bind-key* "C-∃" 'eww)

;; Executing commands
(bind-key* "C-." #'execute-extended-command)
(bind-key* "C-M-." #'execute-extended-command-for-buffer)

;; Consult settings
(bind-key* "C-á" 'consult-buffer)
(bind-key* "M-z" 'recentf)
(unbind-key "C-x C-b")

(bind-key* "M-y" 'consult-yank-pop)


;; Consult register settings
(bind-key* "C-Ϟ" 'consult-register-store)
(bind-key* "C-β" 'consult-register-load)
(bind-key* "C-ψ" 'consult-register)


;; These are also the bookmark settings
(bind-key* "C-&" 'consult-bookmark)
(bind-key* "M-⟨" 'bookmark-set)

;; Embark settings 
(bind-key* "M-a" 'embark-act)


;; Macro settings
(bind-key* "s-a" 'kmacro-start-macro-or-insert-counter)
(bind-key* "s-t" 'kmacro-end-or-call-macro)

(bind-key* "s-e" 'kmacro-name-last-macro)
(bind-key* "s-r" 'insert-kbd-macro)




(bind-key* "s-s" 'desktop-save)

(bind-key* "s-p" 'flyspell-correct-buffer)
(bind-key* "s-<kp-0>" 'cycle-ispell-languages)
(bind-key* "s-1" 'ispell-deutsch)
(bind-key* "s-2" 'ispell-english)

(bind-key* "C-№" 'ivan/cycle-theme)


;; Buffer commands
(bind-key* "C-←" 'previous-buffer)
(bind-key* "C-→" 'next-buffer)


;; Frame commands
(bind-key* "C-↘" 'make-frame-command)
(bind-key* "C-↙" 'delete-frame)
(bind-key* "C-√" 'undelete-frame)
(bind-key* "C-∖" 'other-frame)


;; Tab settings
(bind-key* "C-<" 'tab-previous)
(bind-key* "C->" 'tab-next)

(bind-key* "C-∧" 'tab-new)
(bind-key* "C-∨" 'tab-close)

(bind-key* "C-∂" 'tab-recent)
(bind-key* "C-∫" 'tab-undo)

;; Use Tabgo
(bind-key* "C-z" 'ahp/show-tabs-and-switch)

;; Window settings
(bind-key* "C-𝕀" 'windmove-left)
(bind-key* "C-ℵ" 'windmove-right)

(bind-key* "C-𝔼" 'windmove-up)
(bind-key* "C-𝔸" 'windmove-down)

(bind-key* "C-⨾" 'split-window-right)
(bind-key* "C-ѱ" 'split-window-below)

(bind-key* "C-ℝ" 'delete-other-windows)
(bind-key* "C-ℤ" 'delete-window)

(bind-key* "C-𝕋" 'winner-undo)
(bind-key* "C-ℕ" 'winner-redo)


;; Navigate error messages
(bind-key* "M-ο" 'previous-error)
(bind-key* "M-δ" 'next-error)


;;! Further deletion commands
(bind-key* "M-<backspace>" 'just-one-space)


;; Zoom settings
(bind-key* "C-ο" 'default-text-scale-decrease)
(bind-key* "C-δ" 'default-text-scale-increase)


;; undo-tree-keys
(bind-key* "C-\"" 'undo-tree-undo)
(bind-key* "C-l" 'undo-tree-redo)
(bind-key* "M-j" 'undo-tree-switch-branch)
(bind-key* "M-l" 'undo-tree-visualize)
(bind-key* "C-f M-ä" 'undo-tree-save-state-to-register)
(bind-key* "C-f M-l" 'undo-tree-restore-state-from-register)


;; Avy settings
(bind-key* "C-c" 'ahpjump)
(bind-key* "C-ℂ" 'avy-goto-word-1)
(bind-key* "C-4" 'avy-goto-char-in-line)

(bind-key* "M-χ" 'avy-resume)

(bind-key* "C-<up>" 'avy-goto-line)
(bind-key* "C-5" 'avy-goto-end-of-line)

(bind-key* "C-;" 'avy-prev)
(bind-key* "C-:" 'avy-next)

(bind-key* "M-w" 'avy-kill-ring-save-region)
(bind-key* "M-ω" 'avy-kill-region)

(bind-key* "M-i" 'avy-isearch)


;; We need to do this so the setting doesn't interfere with the edit-server-default.
(with-eval-after-load 'edit-server-edit-mode (bind-key "M-<KP-ENTER>" 'edit-server-done 'edit-server-mode-map))


(bind-key* "M-f" 'free-keys)
(bind-key* "M-j" 'embark-bindings)

(bind-key* "s-C-n" 'all)

;; smudge-mode
(bind-key* "C-α" 'smudge-track-search)
(bind-key* "C-τ" 'ahp/expand-or-smudge)

(bind-key* "C-ε" 'smudge-controller-previous-track)
(bind-key* "C-ρ" 'smudge-controller-next-track)

(bind-key* "C-η" 'smudge-controller-toggle-shuffle)
(bind-key* "C-σ" 'smudge-controller-toggle-repeat)

(bind-key* "C-ς" 'smudge-controller-volume-down)
(bind-key* "C-λ" 'smudge-controller-volume-up)

(bind-key* "C-φ" 'smudge-my-playlists)
(bind-key* "C-ν" 'smudge-playlist-search)
(bind-key* "C-ϟ" 'smudge-create-playlist)
(bind-key* "C-π" 'smudge-recently-played)

(bind-key* "C-φ" 'smudge-select-device)


;; (bind-key* "M-<tab>" 'tab-to-tab-stop)

(bind-key* "M-f" 'arxiv-lookup) 


;; Invoke the all command
(bind-key* "M-s-a" 'all)

;; I use this function to test commands
(bind-key* "M-q" 'ahptest)


;;aya-keybinds
(bind-keys* :prefix-map aya-map :prefix (kbd "C-?")
	    ("e" . aya-create)
	    ("r" . aya-expand-from-history)
	    ("c" . aya-delete-from-history)
	    ("," . aya-clear-history)
	    ("t" . aya-next-in-history)
	    ("a" . aya-previous-in-history)
	    ("p" . aya-persist-snippet)
	    ("f" . aya-open-line)
	    )

(bind-key* "C-!"     #'aya-expand)


(setq wg-prefix-key "C-ẞ")


(bind-keys :prefix-map miscellaneous-map :prefix (kbd "C-`")
	   ("m" . mu4e)
	   ("¬ m" . mu4e-quit)
           ("g" . gnus)
	   ("e" . TeX-command-toggle-shell-escape)
	   ("M" . magit)
	   ("l" . list-packages)	  	   
	   ("v" . vterm)
	   ("s" . shell)
	   ("S" . eshell)           
	   ("f" . flyspell-toggle)
	   ("S" . speed-type-text)
	   ("L" . scroll-lock-mode)
	   ("u" . undo-tree-mode)
	   ("t" . toggle-mode-line)
           ("a" . tab-bar-mode)
	   )

;; Depending on your GUI you might need to uncomment that.
;; (bind-keys :prefix-map miscellaneous-map :prefix (kbd "C-<dead-circumflex>")
;; 	   ("m" . mu4e-modeline-mode)
;; 	   ("¬ m" . mu4e-quit)	   
;; 	   ("s" . TeX-command-toggle-shell-escape)
;; 	   ("M" . magit)
;; 	   ("l" . list-packages)
;; 	   ("v" . vterm)
;; 	   ("f" . flyspell-toggle)
;; 	   ("S" . speed-type-text)
;; 	   ("u" . undo-tree-mode)	   
;; 	   )


;; Evaluation bindings for Elisp
(bind-key "C-⟨" 'eval-defun emacs-lisp-mode-map)
(bind-key "C-—" 'eval-region emacs-lisp-mode-map)
(bind-key "C-M-—" 'eval-buffer emacs-lisp-mode-map)


(with-eval-after-load 'image-load
  (bind-key "a" 'image-previous-file image-mode-map)
  (bind-key "t" 'image-next-file image-mode-map)
  (bind-key "α" 'image-decrease-speed image-mode-map)
  (bind-key "τ" 'image-increase-speed image-mode-map)
  (bind-key "ξ" 'image-reset-speed image-mode-map)
  (bind-key "ζ" 'image-revert-speed image-mode-map)
  )


(bind-key "a" 'help-go-back help-mode-map)
(bind-key "t" 'help-go-forward help-mode-map)

(bind-key "a" 'Info-backward-node Info-mode-map)
(bind-key "t" 'Info-forward-node Info-mode-map)


(bind-key* "M-." 'icicle-goto-marker)

(bind-key* "C-α" 'smudge-track-search)
(bind-key* "C-τ" 'ahp/expand-or-smudge)
(bind-key* "C-ε" 'smudge-controller-previous-track)
(bind-key* "C-ρ" 'smudge-controller-next-track)
(bind-key* "C-η" 'smudge-controller-toggle-shuffle)
(bind-key* "C-σ" 'smudge-controller-toggle-repeat)
(bind-key* "C-ς" 'smudge-controller-volume-down)
(bind-key* "C-λ" 'smudge-controller-volume-up)
(bind-key* "C-μ" 'smudge-my-playlists)
(bind-key* "C-ν" 'smudge-playlist-search)
(bind-key* "C-φ" 'smudge-create-playlist)
(bind-key* "C-΀" 'smudge-recently-played)
(bind-key* "C-φ" 'smudge-select-device)


(bind-keys :prefix-map miscellaneous-map :prefix (kbd "C-`");; (bind-keys :prefix-map miscellaneous-map :prefix (kbd "C-<dead-circumflex>")
	   ("m" . mu4e)
	   ("¬ m" . mu4e-quit)
           ("g" . gnus)
	   ("e" . TeX-command-toggle-shell-escape)
	   ("M" . magit)
	   ("l" . list-packages)	  	   
	   ("v" . vterm)
	   ("s" . shell)
	   ("S" . eshell)           
	   ("f" . flyspell-toggle)
	   ("S" . speed-type-text)
	   ("L" . scroll-lock-mode)
	   ("u" . undo-tree-mode)
	   ("t" . toggle-mode-line)
           ("a" . tab-bar-mode)
	   )
