(add-to-list 'load-path "~/.emacs.d/help+")
(let ((default-directory  "/home/alex/.emacs.d/site-lisp/"))
  (normal-top-level-add-subdirs-to-load-path))
(add-to-list 'load-path "~/.emacs.d/site-lisp/")
(add-to-list 'load-path "~/.emacs.d/site-lisp/DEmacs/")
(add-to-list 'load-path "~/.emacs.d/site-lisp/DEmacs/DEmacs-basic/")

;; (add-to-list 'load-path "~/.emacs.d/site-lisp/w3m/")
;; (add-to-list 'load-path "~/.emacs.d/site-lisp/emacs-application-framework")

(load "~/.emacs.d/site-lisp/icicles-install")

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(with-eval-after-load 'package
  (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/")))

(add-to-list 'default-frame-alist '(fullscreen . maximized)) 

(desktop-save-mode 1)
(setq desktop-path '("~/.emacs.d/desktops/"))

(setq browse-url-secondary-browser-function 'browse-url-firefox)

(load (expand-file-name "~/.roswell/helper.el"))
(setq inferior-lisp-program "ros -Q run")

(scroll-bar-mode -1)
(tool-bar-mode -1)

;; This is taken and modified from here: https://github.com/patrickt/emacs/blob/master/readme.org

;; Set up garbage collection
(setq gc-cons-threshold 100000000)

(setq
 ;; No need to remind me what a scratch buffer is.
 initial-scratch-message nil
 ;; Double-spaces after periods is morally wrong.
 sentence-end-double-space nil
 ;; Never ding at me, ever.
 ring-bell-function 'ignore
 ;; Save existing clipboard text into the kill ring before replacing it.
 save-interprogram-paste-before-kill t
 ;; Fix undo in commands affecting the mark.
 mark-even-if-inactive nil
 ;; Let C-k delete the whole line.
 kill-whole-line t
 ;; accept 'y' or 'n' instead of yes/no
 use-short-answers t
 ;; eke out a little more scrolling performance
 fast-but-imprecise-scrolling t
 ;; prefer newer elisp files
 load-prefer-newer t
 ;; when I say to quit, I mean quit
 confirm-kill-processes nil
 ;; I want to close these fast, so switch to it so I can just hit 'q'
 help-window-select t
 ;; keep the point in the same place while scrolling
 scroll-preserve-screen-position t
 ;; more info in completions
 completions-detailed t
 ;; don't let the minibuffer muck up my window tiling
 read-minibuffer-restore-windows t
 ;; don't keep duplicate entries in kill ring
 kill-do-not-save-duplicates t
 )

;; Never mix tabs and spaces. Never use tabs, period.
;; We need the setq-default here because this becomes
;; a buffer-local variable when set.
(setq-default indent-tabs-mode nil)

;; For unicode
(set-charset-priority 'unicode)
(prefer-coding-system 'utf-8-unix)

(setq-default mode-line-format nil) 


;; Diary settings
(setq diary-file '"~/.emacs.d/diary")
(setq calendar-set-date-style '"european")
(setq diary-mail-addr '"ahprae@protonmail.com")


;;This was taken from here: http://alhassy.com/emacs.d/
;; New location for backups.
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

;; Silently delete execess backup versions
(setq delete-old-versions t)

;; Only keep the last 1000 backups of a file.
(setq kept-old-versions 1000)

;; Use version numbers for backup files.
(setq version-control t)


;; Make Emacs backup everytime I save

(defun my/force-backup-of-buffer ()
  "Lie to Emacs, telling it the curent buffer has yet to be backed up."
  (setq buffer-backed-up nil))

(add-hook 'before-save-hook  'my/force-backup-of-buffer)

;; [Default settings]
;; Autosave when idle for 30sec or 300 input events performed
(setq auto-save-timeout 30
      auto-save-interval 300)


;; Font settings
(setq font-use-system-font t)
(set-face-attribute 'default nil :height 125)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8-unix)



(set-frame-font "JetBrainsMono Nerd Font" nil t nil)
