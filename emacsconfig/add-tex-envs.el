;; add-tex-envs.el --- Add-Tex-Envs                 -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Alexander Prähauser

;; Author: Alexander Prähauser <ahprae@protonmail.com>
;; Keywords: tex, convenience, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides a macro to add environments to AucTeX, RefTeX, CDLaTeX at the same time. it takes as an input a list of tuples of two strings, the environment abbreviation and the full environment name and it adds this list to all environment lists of the three programs, as long as neither the abbreviation nor the complete name are in any of the lists.

;; Example:

;; (add-tex-envs ("axm" "Axiom"))

;; This adds


;;; Code:



(provide 'add-tex-envs)
;;; add-tex-envs.el ends here

(require 'latex)
(require 'cdlatex)
(require 'reftex)

(defun addtexenvs-add-cdlatex-envs (inputlist)
  "Adds latex-environments to cdlatex-env-alist."
  (mapcar
   #'(lambda
       (envtuple)
       (cond
        ((some #'identity
               (mapcar
                #'(lambda
                    (cdlatexenv)                             
                    (cl-member
                     (car envtuple)
                     cdlatexenv
                     :test #'string=)
                    )
                cdlatex-env-alist))
         (message "%s is already defined in cdlatex-env-alist." (car envtuple)))
        ((some #'identity
               (mapcar
                #'(lambda
                    (cdlatexenv)
                    (string-match-p
                     (nth 0 envtuple)
                     (nth 1 cdlatexenv)
                     ))
                cdlatex-env-alist))
         (message "%s is already taken in cdlatex-env-alist." (nth 1 envtuple)))        
        (t
         (add-to-list 'cdlatex-env-alist
                      (list                           
                       (car envtuple)                           
                       (concat
                        "\\begin{"                           
                        (nth 0 envtuple)                           
                        "}\n  AUTOLABEL\n  ?\n\\end{"                            
                        (nth 0 envtuple)
                        "}\n")
                       nil)))
        ))
   inputlist
   )
  )



(defun addtexenvs-add-cdlatex-command-list (inputlist)
  "Adds latex-environments to cdlatex-command-alist."
  (mapcar
   #'(lambda
       (envtuple)
       (cond
        ((some #'identity
               (mapcar
                #'(lambda
                    (cdlatexcmd)                             
                    (string=
                     (nth 1 envtuple)
                     (nth 0 cdlatexcmd)
                     ))
                cdlatex-command-alist))
         (message "%s is already defined in cdlatex-command-alist." (car envtuple)))
        ((some #'identity
               (mapcar
                #'(lambda
                    (cdlatexcmd)
                    (string-match-p
                     (nth 1 envtuple)
                     (car (nth 4 cdlatexcmd))
                     ))
                cdlatex-command-alist))
         (message "%s is already taken in cdlatex-command-alist." (nth 1 envtuple)))
        (t   (add-to-list 'cdlatex-command-alist
                          (list                           
                           (nth 1 envtuple)                           
                           (concat
                            "Insert "                           
                            (nth 0 envtuple)                           
                            " env")
                           '""
                           'cdlatex-environment
                           (list (nth 0 envtuple))
                           t
                           nil))                        
             )
        ))
   inputlist
   
   )
  )


(defun addtexenvs-add-my-auctex-environments (inputlist)
  "Adds latex-environments to AucTeX environments."
  (interactive)
  (mapcar
   #'(lambda (envtuple)
       (LaTeX-add-environments
        (list (nth 0 envtuple) 'LaTeX-env-label)))
   inputlist)
  )


(defun addtexenvs-add-reftex-envs (inputlist)
  "Adds latex-environments to reftex-label-alist."
  (if (not (boundp 'reftex-label-alist))
      (setq reftex-label-alist (list nil))
    )
  (mapcar
   #'(lambda
       (envtuple)
       (cond
        ((and (boundp 'reftexenv)
              (some #'identity
                    (mapcar
                     #'(lambda
                         (reftexenv)                             
                         (string=
                          (nth 0 envtuple)
                          (nth 0 reftexenv)
                          ))
                     reftex-label-alist)))
         (message "%s is already defined in reftex-label-alist." (car envtuple)))
        ((and (boundp 'reftexenv)
              (some #'identity
                    (mapcar
                     #'(lambda
                         (reftexenv)
                         (if (nth 2 reftexenv)
                             (string-match-p
                              (nth 1 envtuple)
                              (nth 2 reftexenv)
                              )))
                     reftex-label-alist)))
         (message "%s is already taken in reftex-label-alist." (nth 1 envtuple)))
        ((and (boundp 'reftexenv)
              (>= (length envtuple) 3)
              (not (identity (nth 3 envtuple)))
              (some #'identity
                    (mapcar
                     #'(lambda
                         (reftexenv)
                         (char-equal
                          (nth 2 envtuple)
                          (nth 1 reftexenv)
                          ))
                     reftex-label-alist)))
         (message "%s is already taken in reftex-label-alist." (nth 2 envtuple)))
        (t   (add-to-list 'reftex-label-alist
                          (list                           
                           (nth 0 envtuple)
                           (nth 2 envtuple) 
                           (concat (nth 1 envtuple) ":")
                           '"~\\ref{%s}"
                           nil
                           (list
                            (nth 0 envtuple)
                            (concat
                             (nth 0 envtuple)
                             '".")
                            )))                        
             )                     
        ))
   inputlist
   ) 
  )

(defun addtexenvs-add (inputlist)
  "This function adds environments given to it in the form of a list of tuples of an environment name and an abbreviation to the CDLaTeX, AUCTeX, RefTeX lists."
  ;;  (setq reftex-label-alist nil)
  (addtexenvs-add-cdlatex-envs inputlist)
  (addtexenvs-add-cdlatex-command-list inputlist)
  (addtexenvs-add-my-auctex-environments inputlist)
  (addtexenvs-add-reftex-envs inputlist)
  )

;; (defconst tex-envs
;;   (list
;;    (list '"Theorem" '"thm" ?a)
;;    (list '"Axiom" '"axm" ?b)
;;    (list '"Definition" '"dfn" ?a t)
;;    (list '"Proposition" '"prp" ?a t)
;;    (list '"Lemma" '"lmm" ?a t)
;;    (list '"Conjecture" '"cnj" ?a t)
;;    (list '"Remark" '"rmk" ?a t)
;;    (list '"Construction" '"cst" ?a t)
;;    (list '"Variation" '"vrt" ?a t)
;;    (list '"Warning" '"wrn" ?a t)
;;    (list '"Figure" '"fig" ?a t)
;;    (list '"proof" '"prf" ?a t)
;;    (list '"Test" '"test" ?a t)
;;    (list '"tikzcd" '"tic" ?a t)
;;    ))

                                        ;  (defconst test-tex-list
                                        ;   st
                                        ;  (list (list '"Test2" '"Test2")))

;; (addtexenvs-add test-tex-list)
;; (addtexenvs-add-reftex-envs test-tex-list)



(provide 'add-tex-envs)
