;; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))


(defun disable-all-minor-modes ()
  (interactive)
  (mapc
   (lambda (mode-symbol)
     (when (functionp mode-symbol)
       ;; some symbols are functions which aren't normal mode functions
       (ignore-errors 
         (funcall mode-symbol -1))))
   minor-mode-list))


(defun ahptest ()
  "I use this function to test things."
  (interactive)
  (if (>= corfu--index 0)
      (corfu-next))
  ) 


(defun ahpjump ()
  "This function triggers avy  when not in a minibuffer and vertico-quick-jump otherwise."
  (interactive)
  (if (derived-mode-p 'minibuffer-mode)
      (vertico-quick-jump)
    (call-interactively 'avy-goto-char-timer)))


(defun ahpnoword () 
  "Return true if point is on a character that suggests it's not in a word"
  (interactive)
  (if (or (memq (following-char) '(?  ?\( ?\) ?\[ ?\] ?{ ?} ?\< ?\> ?. ?, ?\" ?\$ ?`)) (eolp))
      t
    ))


(defun ahpfwd ()
  "This function chooses the current Vertico-entry when you're at the end of a minibuffer, the current Corfu-entry when you're at the end of a word or line in a normal buffer and goes a character forward otherwise."
  (interactive)
  (cond ((derived-mode-p 'minibuffer-mode)
         (if (eobp)
             (vertico-directory-enter)
           (forward-char)))
        ((>= corfu--index 0)
         (if (ahpnoword)
             (corfu--insert 'finished)
           (forward-char)))
        ((derived-mode-p 'pdf-view-mode)
         (image-forward-hscroll 1))
        ((and (derived-mode-p 'vterm-mode) (not (bound-and-true-p vterm-copy-mode)))
         (vterm-send-right))
        ((derived-mode-p 'bookmark-bmenu-mode)
         (bookmark-bmenu-this-window))
        ((derived-mode-p 'reftex-select-label-mode)
         (reftex-select-accept))
        ((and (derived-mode-p 'dired-mode) (derived-mode-p 'dired-mode))
         (dired-find-file))
        ((derived-mode-p 'reftex-toc-mode)
         (reftex-toc-goto-line-and-hide))
        ((derived-mode-p 'gnus-summary-mode)
         (gnus-summary-next-page))
        ((derived-mode-p 'gnus-group-mode)
         (gnus-group-select-group t))
        ((derived-mode-p 'smudge-track-search-mode)
         (smudge-track-select))
        ((derived-mode-p 'image-mode)
         (call-interactively 'image-next-file))
        (t (forward-char))
        ))


(defun ahpbwd ()
  "This function quits Corfu if any Corfu-entries are shown and otherwise behaves like the left arrow key."
  (interactive)
  (cond
   ;; ((derived-mode-p 'minibuffer-mode)
   ;;  (if (bobp) (call-interactively 'vertico-exit-input)))
   ((>= corfu--index 0)
    (corfu-quit))
   ((derived-mode-p 'pdf-view-mode)
    (image-backward-hscroll 1))
   ;; (isearch-mode
   ;;  (isearch-edit-string))
   ((and (derived-mode-p 'vterm-mode) (not (bound-and-true-p vterm-copy-mode)))
    (vterm-send-left))
   ((derived-mode-p 'bookmark-bmenu-mode)
    (bmkp-bmenu-quit))
   ((derived-mode-p 'reftex-select-label-mode)
    (reftex-select-quit))
   ((and (derived-mode-p 'dired-mode) (derived-mode-p 'dired-mode))
    (diredp-up-directory))
   ((derived-mode-p 'reftex-toc-mode)
    (reftex-toc-quit))
   ((derived-mode-p 'gnus-summary-mode)
    (gnus-summary-exit))
   ((derived-mode-p 'gnus-group-mode)
    (gnus-group-exit))
   ((derived-mode-p 'smudge-track-search-mode)
    (quit-window))
   ((derived-mode-p 'image-mode)
    (call-interactively 'image-previous-file))
   (t
    ;; (progn
    ;;   (let ((pos (point))) 
    (backward-char)
    ;; (if
    ;;     (and
    ;;      (derived-mode-p 'minibuffer-mode)
    ;;      (equal pos (point)))
    ;;     (vertico-exit-input)
    ;;   (message "Value %s %s" (point) pos)
    ;;   )
    ;; ))
   )))


(defun ahpup ()
  "This function behaves like the up arrow key."
  (interactive)
  (cond
   ((and (derived-mode-p 'vterm-mode) (not (bound-and-true-p vterm-copy-mode)))
    (vterm-send-up))
   ((derived-mode-p 'minibuffer-mode)
    (vertico-previous))
   ((>= corfu--index 0)
    (corfu-previous))        
   ((derived-mode-p 'pdf-view-mode)
    (pdf-view-scroll-down-or-previous-page 1))
   ((derived-mode-p 'image-mode)
    (call-interactively 'image-previous-line))
   (t (previous-line))
   ))


(defun ahpdown ()
  "This function behaves like the down arrow key."
  (interactive)
  (cond ((and (derived-mode-p 'vterm-mode) (not (bound-and-true-p vterm-copy-mode)))
         (vterm-send-down))
        ((derived-mode-p 'minibuffer-mode)
         (vertico-next))
        ((>= corfu--index 0)
         (corfu-next))                
        ((derived-mode-p 'pdf-view-mode)
         (pdf-view-scroll-up-or-next-page 1))        
        ((derived-mode-p 'image-mode)
         (call-interactively 'image-next-line))
        (t (next-line))
        ))


(defun ahpprevious ()
  "This function behaves like the previous key."
  (interactive)
  (cond ((derived-mode-p 'pdf-view-mode)
         (pdf-view-previous-page-command))
        ((or (derived-mode-p 'minibuffer-mode) (derived-mode-p 'isearch-mode))
         (previous-line-or-history-element))
        ((derived-mode-p 'image-mode)
         (call-interactively 'image-previous-frame))
        (t (scroll-down-command))
        ))

  
(defun ahpnext ()
  "This function behaves like the next key."
  (interactive)
  (cond ((derived-mode-p 'pdf-view-mode)
         (pdf-view-next-page-command))
        ((or (derived-mode-p 'minibuffer-mode) (derived-mode-p 'isearch-mode))
         (next-line-or-history-element))
        ((derived-mode-p 'image-mode)
         (call-interactively 'image-next-frame))
        (t (scroll-up-command))
        ))


(defun ahphome ()
  "This function behaves like the home key."
  (interactive)
  (cond ((derived-mode-p 'pdf-view-mode)
         (pdf-view-scroll-down-or-previous-page 100))
        (t (beginning-of-visual-line 1))
        ))


(defun ahpend ()
  "This function behaves like the end key."
  (interactive)
  (cond ((derived-mode-p 'pdf-view-mode)
         (pdf-view-scroll-up-or-next-page 100))
        (t (end-of-visual-line 1))
        ))


(defun ahp/expand-or-smudge ()
  "This function expands a yasnippet if one exists, expands a cdlatex-item if one exists and toggles spotify on or off otherwise."
  (interactive)
  (if (yas-maybe-expand-abbrev-key-filter t)
      (yas-expand)
    (let ((pos (point)) exp math-mode)
      (backward-word 1)
      (while (eq (following-char) ?$) (forward-char 1))
      (setq exp (buffer-substring-no-properties (point) pos))
      (setq exp (assoc exp cdlatex-command-alist-comb))
      (cond
       (exp
        (progn
          (setq math-mode (cdlatex--texmathp))
          (when (or (and (not math-mode) (nth 5 exp))
                    (and math-mode (nth 6 exp)))
            (delete-char (- pos (point)))
            (insert (nth 2 exp))
            (and (nth 3 exp)
                 (if (nth 4 exp)
                     (apply (nth 3 exp) (nth 4 exp))
                   (funcall (nth 3 exp)))))))
       (t
        (progn
          (goto-char pos)
          (call-interactively 'smudge-controller-toggle-play)))
       ))))


(defun ahp/expand ()
  "This function expands a yasnippet if one exists, expands a cdlatex-item if one exists and toggles spotify on or off otherwise."
  (interactive)
  (if (yas-maybe-expand-abbrev-key-filter t)
      (yas-expand)
    (let ((pos (point)) exp math-mode)
      (backward-word 1)
      (while (eq (following-char) ?$) (forward-char 1))
      (setq exp (buffer-substring-no-properties (point) pos))
      (setq exp (assoc exp cdlatex-command-alist-comb))
      (cond
       (exp
        (progn
          (setq math-mode (cdlatex--texmathp))
          (when (or (and (not math-mode) (nth 5 exp))
                    (and math-mode (nth 6 exp)))
            (delete-char (- pos (point)))
            (insert (nth 2 exp))
            (and (nth 3 exp)
                 (if (nth 4 exp)
                     (apply (nth 3 exp) (nth 4 exp))
                   (funcall (nth 3 exp)))))))
       ))))


(defun ahp/kill-ring-delete-dollars ()
  "This function deletes dollar signs in the highest entry in the kill ring."
  (interactive)
  (let ((new
         (string-replace "$" nil (car kill-ring))))
    (kill-new new t)))


(defun ahp/kill-ring-delete-dollars-number (x)
  "This function deletes dollar signs in the xth entry in the kill ring."
  (interactive "n  Number:")
  (let ((new
         (string-replace "$" nil (nth x kill-ring))))
    (kill-new new t)))


(defalias 'ahp/indent-item
  (kmacro "<kp-enter> C-S-<backspace> C-a"))


(defun ahp/exchange (x y)
  "This function exchanges two strings. The first string is mapped to a placeholder, then the second string to the first string, then the placeholder to the second string."
  (interactive"s Exchange: \ns With:")
  (replace-string x "πλαχηολδερ")
  (beginning-of-buffer)
  (replace-string y x)
  (beginning-of-buffer)
  (replace-string "πλαχηολδερ" y)
  )

;; (defun ahp/kill-all-lines ()
;;   "This function kills all lines in the buffer of a regexp-search using the all-package."
;;   (interactive)
;;   (let lines (- (count-lines (point-min) (point-max) 3))
;;        (let current-line (setq current-line 0)
;;             (while (< current-line lines))
;;             (beginning-of-line)
;;             (setq beg mark)
;;             (end-of-line)
;;             (kill-region)
;;             (next-line)
;;             (setq current-line (+ current-line 1))
;;             )
;;        ))

(defun ahp/kill-all-lines ()
  "This function kills all lines in the buffer of a regexp-search using the all-package."
  (interactive)
  (setq lines (- (count-lines (point-min) (point-max) 3)))
  (setq current-line 0)
  (while (< current-line lines)
    (beginning-of-line)
    (setq beg mark)
    (end-of-line)
    (kill-region)
    (next-line)
    (setq current-line (+ current-line 1))
    )
  )

(defun ahp/show-tabs-and-switch ()
  "Activate Tab-Bar-Mode, switch to a tab with tabgo and deactivate tab-bar-mode."
  (interactive)
  (tab-bar-mode 1)
  (tabgo)
  (tab-bar-mode -1)
  )

;; (defun ahp/toggle-tab-bar ()
;;   "Toggle Tab-Bar-Mode."
;;   (interactive)
;;   (tab-bar-mode 1)
;;   (tabgo)
;;   (tab-bar-mode -1)
;;   )


(defun ahp/kill-append ()
  (interactive)
  (append-next-kill)
  (kill-region (mark) (point)))

(defun previous-frame ()
  "Invoke other-frame with -1."
  (interactive)
  (other-frame 1))


(defun toggle-mode-line ()
  "Toggle mode-line"
  (interactive)
  (if (equal mode-line-format nil)
      (setq mode-line-format
            '("%e" mode-line-front-space
              (:propertize
               ("" mode-line-mule-info mode-line-client mode-line-modified
                mode-line-remote)
               display (min-width (5.0)))
              mode-line-frame-identification mode-line-buffer-identification "   "
              mode-line-position (vc-mode vc-mode) "  " mode-line-modes
              mode-line-misc-info mode-line-end-spaces))
    (setq mode-line-format nil)))



(defun disable-all-minor-modes ()
  (interactive)
  (mapc
   (lambda (mode-symbol)
     (when (functionp mode-symbol)
       ;; some symbols are functions which aren't normal mode functions
       (ignore-errors 
         (funcall mode-symbol -1))))
   minor-mode-list))


;; This was taken from here: https://emacs.stackexchange.com/a/79931/39786
(defun MetaSuper-next-cmd ()
  (interactive)
  (execute-kbd-macro
   (vector
    (event-apply-modifier
     (event-apply-modifier
      (read-event) 'super 23 "s-")
     'meta 27 "M-"))))


(defun amurray-LaTeX-delete-environment ()
  (interactive)
  (when (LaTeX-current-environment)
    (save-excursion
      (let* ((begin-start (save-excursion
                            (LaTeX-find-matching-begin)
                            (point)))
             (begin-end (save-excursion
                          (goto-char begin-start)
                          (search-forward-regexp "begin{.*?}")))
             (end-end (save-excursion
                        (LaTeX-find-matching-end)
                        (point)))
             (end-start (save-excursion
                          (goto-char end-end)
                          (1- (search-backward-regexp "\\end")))))
        ;; delete end first since if we delete begin first it shifts the
        ;; location of end
        (delete-region end-start end-end)
        (delete-region begin-start begin-end)))))


(defsubst compose (function &rest more-functions)
  (cl-reduce (lambda (f g)
               (lexical-let ((f f) (g g))
                 (lambda (&rest arguments)
                   (funcall f (apply g arguments)))))
             more-functions
             :initial-value function))


(defun chromosome-compose (llist rlist)
  (cl-mapcar 'list llist rlist))


(defun inverse-chromosome-compose (tlist)
  (list (cl-mapcar (lambda (x) (nth 0 x)) tlist) (cl-mapcar (lambda (y) (nth 1 y)) tlist)))


;; Define functions switching between Latex and Luatex
(defconst upgreek (list (list '"\\\\Gamma" '"Γ") (list '"\\\\Delta" '"Δ") (list '"\\\\Theta" '"Θ") (list '"\\\\Lambda" '"Λ") (list '"\\\\Xi" '"Ξ") (list '"\\\\Pi" '"Π")
                        (list '"\\\\Sigma" '"Σ") (list '"\\\\Phi" '"Φ") (list '"\\\\Psi" '"Ψ") (list '"\\\\Omega" '"Ω") (list '"\\\\Heta" '"Ͱ")))

(defconst lowgreek (list (list '"\\\\α" '"α") (list '"\\\\beta" '"β") (list '"\\\\gamma" '"γ") (list '"\\\\delta" '"δ") (list '"\\\\epsilon" '"ε")
 (list '"\\\\zeta" '"ζ") (list '"\\\\eta" '"η") (list '"\\\\theta" '"θ") (list '"\\\\iota" '"ι") (list '"\\\\kappa" '"κ") (list '"\\\\lambda" '"λ")
 (list '"\\\\mu" '"μ") (list '"\\\\nu" '"ν") (list '"\\\\xi" '"ξ") (list '"\\\\pi" '"π") (list '"\\\\rho" '"ρ") (list '"\\\\sigma" '"σ")
 (list '"\\\\varsigma" '"ς") (list '"\\\\tau" '"τ") (list '"\\\\upsilon" '"υ") (list '"\\\\phi" '"φ") (list '"\\\\chi" '"χ") (list '"\\\\psi" '"ψ")
 (list '"\\\\omega" '"ω") (list '"\\\\heta" '"ͱ")))


(defconst cyrsymb (list (list '"\\\\mttoa{\\\\cyrb}" '"б") (list '"\\\\mttoa{\\\\cyrg}" '"г") (list '"\\\\mttoa{\\\\cyrd}" '"д") (list '"\\\\mttoa{\\\\cyrzh}" '"ж") (list '"\\\\mttoa{\\\\cyrz}" '"э") (list '"\\\\mttoa{\\\\cyri}" '"и")
 (list '"\\\\mttoa{\\\\cyr}" '"к") (list '"\\\\mttoa{\\\\cyrl}" '"л") (list '"\\\\mttoa{\\\\cyrm}" '"м") (list '"\\\\mttoa{\\\\cyrn}" '"н") (list '"\\\\mttoa{\\\\cyro}" '"о") (list '"\\\\mttoa{\\\\cyrp}" '"п")
 (list '"\\\\mttoa{\\\\cyrr}" '"р") (list '"\\\\mttoa{\\\\cyrs}" '"с") (list '"\\\\mttoa{\\\\cyrt}" '"т") (list '"\\\\mttoa{\\\\cyru}" '"у") (list '"\\\\mttoa{\\\\cyrf}" '"ф") (list '"\\\\mttoa{\\\\cyrh}" '"х")
 (list '"\\\\mttoa{\\\\cyrc}" '"ц") (list '"\\\\mttoa{\\\\cyrch}" '"ч") (list '"\\\\mttoa{\\\\cyrsh}" '"ш") (list '"\\\\mttoa{\\\\cyrshch}" '"щ") (list '"\\\\mttoa{\\\\cyrhrdsn}" '"ъ")
 (list '"\\\\mttoa{\\\\cyrery}" '"ы") (list '"\\\\mttoa{\\\\cyrsftsn}" '"ь") (list '"\\\\mttoa{\\\\cyryu}" '"ю") (list '"\\\\mttoa{\\\\cyrya}" '"я") (list '"\\\\mttoa{\\\\cyrdje}" '"ђ")
 (list '"\\\\mttoa{\\\\cyrnje}" '"њ") (list '"\\\\mttoa{\\\\cyrdzhe}" '"џ")))
 

(defconst mathl6 (list (list '"\\\\mathbb{E}" '"𝔼") (list '"\\\\exists" '"∃") (list '"\\\\mathbb{Q}" '"ℚ") (list '"\\\\mathbb{V}" '"𝕍")
                       (list '"\\\\mathbb{C}" '"ℂ") (list '"\\\\mathbb{M}" '"𝕄") (list '"\\\\mathbb{J}" '"𝕁") (list '"\\\\mathbb{H}" '"ℍ")
                       (list '"\\\\mathbb{I}" '"𝕀") (list '"\\\\forall" '"∀") (list '"\\\\aleph" '"ℵ") (list '"\\\\grad" '"∇")
                       (list '"\\\\mathbb{T}" '"𝕋") (list '"\\\\mathbb{R}" '"ℝ") (list '"\\\\mathbb{N}" '"ℕ")
                       (list '"\\\\mathbb{K}" '"𝕂") (list '"\\\\mathbb{Y}" '"𝕐") (list '"\\\\beth" '"ב") (list '"\\\\mathbb{Z}" '"ℤ")))


;; (defconst mathl6 (list (list '"$\\\\mathbb{E}$" '"𝔼") (list '"\\\\exists" '"∃") (list '"$\\\\mathbb{Q}$" '"ℚ") (list '"$\\\\mathbb{V}$" '"𝕍")
;;  (list '"$\\\\mathbb{C}$" '"ℂ") (list '"$\\\\mathbb{M}$" '"𝕄") (list '"\\\\mathbb{J}" '"𝕁") (list '"$\\\\mathbb{H}$" '"ℍ")
;;  (list '"$\\\\mathbb{I}$" '"𝕀") (list '"\\\\forall" '"∀") (list '"\\\\aleph" '"ℵ") (list '"\\\\grad" '"∇")
;;  (list '"$\\\\mathbb{T}$" '"𝕋") (list '"$\\\\mathbb{R}$" '"ℝ") (list '"$\\\\mathbb{N}$" '"ℕ")
;;  (list '"$\\\\mathbb{K}$" '"𝕂") (list '"$\\\\mathbb{Y}$" '"𝕐") (list '"\\\\beth" '"ב") (list '"$\\\\mathbb{Z}$" '"ℤ")))


(defconst mathl7 (list (list '"\\\\times" '"×") (list '"\\\\nwarrow" '"↖") (list '"\\\\uparrow" '"↑") (list '"\\\\nearrow" '"↗") (list '"\\\\ltimes" '"⋉")
                       (list '"\\\\rtimes" '"⋊") (list '"\\\\prec" '"≺") (list '"\\\\wedge" '"∧") (list '"\\\\succ" '"≻") (list '"\\\\prod" '"∏")
                       (list '"\\\\coprod" '"∐") (list '"\\\\leftrightarrow" '"↔") (list '"\\\\leftarrow" '"←") (list '"\\\\downarrow" '"↓")
                       (list '"\\\\rightarrow" '"→") (list '"\\\\mapsto" '"↦") (list '"\\\\partial" '"∂") (list '"\\\\vee" '"∨") (list '"\\\\circ" '"∘")
                       (list '"\\\\sum" '"∑") (list '"\\\\swarrow" '"↙") (list '"\\\\diamond" '"⋄") (list '"\\\\varnothing" '"∅") (list '"\\\\searrow" '"↘")
                       (list '"\\\\in " '"∈ ") (list '"\\\\infty" '"∞") (list '"\\\\sqrt" '"√") (list '"\\\\parallel" '"∥") (list '"\\\\int" '"∫")))


(defconst ahptipa (list (list '"\\\\msci" '"ɪ") (list '"\\\\mce" '"ɞ") (list '"\\\\mthorn" '"þ") (list '"\\\\mdh" '"ð") (list '"\\\\mglotstop" '"ʔ") (list '"\\\\doublebarpipe" '"ǂ")))

(defconst ahpdiacritics (list (list '"\\\\check" '"ˇ")))


(defconst ahpmath (append lowgreek upgreek cyrsymb mathl6 mathl7 ahptipa ahpdiacritics))


(defun ahptextouni ()
  "Replace tex-macros with Unicode characters"
  (interactive)
  (cl-mapcar (lambda (x) (progn (mark-whole-buffer) (replace-regexp (nth 0 x) (nth 1 x)) (princ (nth 0 x)))) ahpmath))

(defun ahpunitotex ()
  "Replace tex-macros with Unicode characters"
  (interactive)
  (cl-mapcar (lambda (x) (progn (mark-whole-buffer) (replace-regexp (nth 1 x) (nth 0 x)) (princ (nth 0 x)))) ahpmath))
 


;; From here
(setq ivan/themes '(sanityinc-tomorrow-bright sanityinc-tomorrow-day modus-vivendi))
(setq ivan/themes-index 0)

(defun ivan/cycle-theme ()
  (interactive)
  (setq ivan/themes-index (% (1+ ivan/themes-index) (length ivan/themes)))
  (ivan/load-indexed-theme))

(defun ivan/load-indexed-theme ()
  (ivan/try-load-theme (nth ivan/themes-index ivan/themes)))

(defun ivan/try-load-theme (theme)
  (if (ignore-errors (load-theme theme :no-confirm))
      (mapcar #'disable-theme (remove theme custom-enabled-themes))
    (message "Unable to find theme file for ‘%s’" theme)))




(defun ahp/add-or-set (inputelement inputlist)
  "This function adds an element to a list if the list exists and defines the list with that element otherwise."
  (if (boundp inputlist)
      (add-to-list inputelement inputlist)
    (setq inputlist inputelement)))



(defun jlp/add-to-list-multiple (list to-add)
  "Adds multiple items to LIST.
Allows for adding a sequence of items to the same list, rather
than having to call `add-to-list' multiple times."
  (interactive)
  (dolist (item to-add)
    (add-to-list list item)))


(defun append-to-list (list-var elements)
  "Append ELEMENTS to the end of LIST-VAR.

The return value is the new value of LIST-VAR."
  (unless (consp elements)
    (error "ELEMENTS must be a list"))
  (let ((list (symbol-value list-var)))
    (if list
        (setcdr (last list) elements)
      (set list-var elements)))
  (symbol-value list-var))


(defalias 'ahp/tmp-kill-append-line
  (kmacro "<XF86Launch6> , C-d <XF86Launch6> C-w C-Ω C-r"))

