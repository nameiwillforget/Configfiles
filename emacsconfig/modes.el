(global-undo-tree-mode)
(delete-selection-mode)
(yas-global-mode)
;; (global-company-mode)

;; (helm-mode 1)
;; (icy-mode 1)

(vertico-mode)
(global-corfu-mode)
(vertico-prescient-mode)
(corfu-prescient-mode)

(wc-mode)
(global-visual-line-mode t)
(electric-pair-mode)
(savehist-mode 1)
(dynamic-completion-mode 1)
(backward-forward-mode 1)
(global-prettify-symbols-mode 1)
(smart-mark-mode)
;; (bufler-mode 1)
(undelete-frame-mode)
(auto-insert-mode t)

(auto-fill-mode -1)

;; Temporarily, while eldoc-mode is buggy
(global-eldoc-mode -1)

;; (workgroups-mode 1)
(all-the-icons-completion-mode)
;; (global-display-line-numbers-mode 1)

;; This is taken and modified from here: https://github.com/patrickt/emacs/blob/master/readme.org
;; (mood-line-mode)
(global-goto-address-mode)



;; Aggressive Intent mode for all but some chosen programming modes
(global-aggressive-indent-mode 1)
;; (add-to-list 'aggressive-indent-excluded-modes 'html-mode)

;; Have a fancy compilation
(fancy-compilation-mode)

;; (add-hook 'TeX-mode-hook #'rainbow-delimiters-mode)
;; (add-hook 'TeX-mode-hook #'paredit-everywhere-mode)

;; (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
;; (add-hook 'prog-mode-hook #'paredit-everywhere-mode)


;; (add-hook 'TeX-mode-hook #'undo-tree-mode)
;; (add-hook 'TeX-mode-hook #'dedicated-mode)
;; (add-hook 'LaTeX/MP-mode-hook #'dedicated-mode)

;; (add-hook 'latex-mode-hook #'undo-tree-mode)
;; (add-hook 'latex-mode-hook #'dedicated-mode)


;; (add-hook 'mu4e-compose-mode-hook #'undo-tree-mode)
;; (add-hook 'mu4e-compose-mode-hook #'dedicated-mode)

;; (add-hook 'org-mode-hook #'undo-tree-mode)
;; (add-hook 'org-mode-hook #'dedicated-mode)

;; (add-hook 'vterm-mode-hook #'undo-tree-mode)
;; (add-hook 'vterm-mode-hook #'dedicated-mode)

(add-hook 'text-mode-hook #'rainbow-delimiters-mode)
(add-hook 'text-mode-hook #'paredit-everywhere-mode)
(add-hook 'text-mode-hook #'undo-tree-mode)
;; (add-hook 'text-mode-hook #'dedicated-mode)

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook #'paredit-everywhere-mode)
(add-hook 'prog-mode-hook #'undo-tree-mode)
;; (add-hook 'prog-mode-hook #'dedicated-mode)


;; (add-hook 'conf-unix-mode-hook #'undo-tree-mode)
(add-hook 'minibuffer-mode #'undo-tree-mode)

(add-hook 'pdf-view-mode #'pdf-links-minor-mode)


;; (add-hook 'plain-TeX-mode-hook
;;           #'auctex-cluttex-mode)
;; (add-hook 'LaTeX-mode-hook
;;           #'auctex-cluttex-mode)


;; (add-hook 'TeX-mode-hook 'ac-latex-mode-setup)

(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)






;(flyspell-lazy-mode 1)
(add-hook 'flyspell-mode-hook (lambda () (auto-dictionary-mode 1)))


(epa-file-enable)

(edit-server-start)

(setq prettify-symbols-unprettify-at-point 'right-edge)

(setq winner-mode t)
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

(recentf-mode 1)
(setq-default recent-save-file "~/.emacs.d/recentf")

;; (setq helm-ff-file-name-history-use-recentf t)

;; (put 'upcase-region 'disabled nil) 

;; (setq smudge 1)



(dired-quick-sort-setup)


(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

;; (eval-after-load "isearch" '(require 'isearch+))


(define-derived-mode writer-mode text-mode "W-EN"
  "Writer mode."
  (abbrev-mode 1))

(define-derived-mode writer-english-mode writer-mode "W-EN"
  "Writer mode - English.")

(define-derived-mode writer-german-mode writer-mode "W-DE"
  "Writer mode - German.")

(defun writer- ()
  (interactive)
  (writer-german-mode)
  (set-input-method 'german-postfix)
  (setq header-line-format " DE> ")
  (setq mode-line-format nil)
  (ispell-deutsch)
  (flyspell-mode -1)
  (writer-setup-bindings))

(defun writer-english ()
  (interactive)
  (writer-english-mode)
  (deactivate-input-method)
  (setq header-line-format " EN> ")
  (setq mode-line-format nil)
  (ispell-english)
;  (flyspell-mode 1)
  (writer-setup-bindings))




(define-derived-mode abbrev-org-mode org-mode "O-EN"
  "Org mode."
  (abbrev-mode 1))

(define-derived-mode org-english-mode org-mode "O-EN"
  "Org mode - English.")

(define-derived-mode org-german-mode org-mode "O-DE"
  "Org mode - German.")

(defun org- ()
  (interactive)
  (org-german-mode)
  (set-input-method 'german-postfix)
  (setq header-line-format " DE> ")
  (setq mode-line-format nil)
  (ispell-deutsch)
  (flyspell-mode -1)
  (org-setup-bindings)
  )

;; (let ((org-german-mode-map (make-sparse-keymap)))
;;   (set-keymap-parent org-german-mode-map org-mode-map)
;;   (define-key org-german-mode-map (kbd "\"") '\ü)
;; )



(defun org-english ()
  (interactive)
  (org-english-mode)
  (deactivate-input-method)
  (setq header-line-format " EN> ")
  (setq mode-line-format nil)
  (ispell-english)
;  (flyspell-mode 1)
  (org-setup-bindings))


(menu-bar-mode -1) 
(scroll-bar-mode -1) 
(tool-bar-mode -1) 
 
(add-to-list 'auto-mode-alist '("\\.de\\'" . org-german-mode))
